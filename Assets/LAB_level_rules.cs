using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LAB_level_rules : MonoBehaviour
{
    public List<LAB_Floor> FloorPanels;
    public TriggerInterface OnLevelCompleteTrigger;
    public bool levelCompleted = false;
    int goal;
    int count = 0;
    GameObject[] gameObjects;

    // Start is called before the first frame update
    void Awake()
    {        
        gameObjects = GameObject.FindGameObjectsWithTag("LAB_Floor");
        Debug.Log(gameObjects.Length);
        foreach (GameObject c in gameObjects)
        {
            LAB_Floor i = c.GetComponent<LAB_Floor>();
            FloorPanels.Add(i);
        }
        goal = FloorPanels.Count;
    }

    // Update is called once per frame
    void Update()
    {       
        Debug.Log("Goal Value is: " + goal);
        Debug.Log("FloorPanels.Count Value is: " + FloorPanels.Count);

        if (!levelCompleted)
        {
            count = 0;
            foreach (LAB_Floor floor in FloorPanels)
            {
                floor.UpdateTargets();
                if (floor.completed == true)
                {
                    count++;                    
                }
            }
            if (goal == count)
            {
                levelCompleted = true;
                if (OnLevelCompleteTrigger != null)
                {
                    OnLevelCompleteTrigger.Fire();
                }
            }
        }
    }


}
