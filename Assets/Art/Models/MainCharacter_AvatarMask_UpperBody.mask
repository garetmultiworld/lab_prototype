%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: MainCharacter_AvatarMask_UpperBody
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/cMainV
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/cHeadTarget
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/cHeadTarget/cEyes
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/cHeadTarget/cEyes/cEyes_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/elbowIK.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/elbowIK.L/elbowIK.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/elbowIK.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/elbowIK.R/elbowIK.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/kneeIK.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/kneeIK.L/kneeIK.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/kneeIK.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/kneeIK.R/kneeIK.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/coreIK
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/coreIK/coreIK_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/handIK.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/handIK.L/handIK.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/handIK.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/handIK.R/handIK.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cButt.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cButt.L/cButt.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cButt.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cButt.R/cButt.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cThigh.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cThigh.L/cThigh.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cThigh.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/cThigh.R/cThigh.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/cHip.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/cHip.L/cHip.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe1.L/Toe1.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe2.L/Toe2.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe3.L/Toe3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe4.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe4.L/Toe4.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe5.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.L/Knee.L/Shin.L/Foot.L/Toe5.L/Toe5.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/cHip.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/cHip.R/cHip.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe1.R/Toe1.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe2.R/Toe2.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe3.R/Toe3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe4.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe4.R/Toe4.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe5.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Hip.R/Knee.R/Shin.R/Foot.R/Toe5.R/Toe5.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Boob.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Boob.L/Boob.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/boob.L_spring_tail
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Boob.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Boob.R/Boob.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/boob.R_spring_tail
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/cBicep.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/cBicep.L/cBicep.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/cWrist.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/cWrist.L/cWrist.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Middle1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Middle1.L/Middle2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Middle1.L/Middle2.L/Middle3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Middle1.L/Middle2.L/Middle3.L/Middle3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pinky1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pinky1.L/Pinky2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pinky1.L/Pinky2.L/Pinky3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pinky1.L/Pinky2.L/Pinky3.L/Pinky3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pointer1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pointer1.L/Pointer2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pointer1.L/Pointer2.L/Pointer3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Pointer1.L/Pointer2.L/Pointer3.L/Pointer3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Ring1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Ring1.L/Ring2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Ring1.L/Ring2.L/Ring3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Ring1.L/Ring2.L/Ring3.L/Ring3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Thumb1.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Thumb1.L/Thumb2.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Thumb1.L/Thumb2.L/Thumb3.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Bicep.L/Elbow.L/4arm.L/Hand.L/Thumb1.L/Thumb2.L/Thumb3.L/Thumb3.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Shoulder.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.L/Shoulder.L/Shoulder.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/cBicep.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/cBicep.R/cBicep.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/cWrist.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/cWrist.R/cWrist.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Middle1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Middle1.R/Middle2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Middle1.R/Middle2.R/Middle3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Middle1.R/Middle2.R/Middle3.R/Middle3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pinky1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pinky1.R/Pinky2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pinky1.R/Pinky2.R/Pinky3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pinky1.R/Pinky2.R/Pinky3.R/Pinky3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pointer1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pointer1.R/Pointer2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pointer1.R/Pointer2.R/Pointer3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Pointer1.R/Pointer2.R/Pointer3.R/Pointer3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Ring1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Ring1.R/Ring2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Ring1.R/Ring2.R/Ring3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Ring1.R/Ring2.R/Ring3.R/Ring3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Thumb1.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Thumb1.R/Thumb2.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Thumb1.R/Thumb2.R/Thumb3.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Bicep.R/Elbow.R/4arm.R/Hand.R/Thumb1.R/Thumb2.R/Thumb3.R/Thumb3.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Shoulder.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/cShrugger.R/Shoulder.R/Shoulder.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBlink
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBlink/cBlink_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBrowL
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBrowL/cBrowL_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBrowR
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cBrowR/cBrowR_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cMouth
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cMouth/cMouth_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cTongue
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/cTongue/cTongue_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/Eye.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/Eye.L/Eye.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/Eye.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/Eye.R/Eye.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003/h.005
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003/h.005/h.008
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003/h.005/h.008/h.006
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003/h.005/h.008/h.006/h.007
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h/h.001/h.003/h.005/h.008/h.006/h.007/h.007_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.L/h.004.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.L/h.004.L/h.008.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.L/h.004.L/h.008.L/h.008.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.R/h.004.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.R/h.004.R/h.008.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.002.R/h.004.R/h.008.R/h.008.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.009
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.009/h.010
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.009/h.010/h.011
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.009/h.010/h.011/h.012
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.009/h.010/h.011/h.012/h.012_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L/h.014.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L/h.014.L/h.015.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L/h.014.L/h.015.L/h.016.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L/h.014.L/h.015.L/h.016.L/h.017.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.L/h.014.L/h.015.L/h.016.L/h.017.L/h.017.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R/h.014.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R/h.014.R/h.015.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R/h.014.R/h.015.R/h.016.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R/h.014.R/h.015.R/h.016.R/h.017.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.013.R/h.014.R/h.015.R/h.016.R/h.017.R/h.017.R_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019/h.020
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019/h.020/h.021
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019/h.020/h.021/h.022
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019/h.020/h.021/h.022/h.023
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.018/h.019/h.020/h.021/h.022/h.023/h.023_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.024
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.024/h.025
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.024/h.025/h.026
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.024/h.025/h.026/h.027
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.024/h.025/h.026/h.027/h.027_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.028
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.028/h.029
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.028/h.029/h.030
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.028/h.029/h.030/h.031
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.028/h.029/h.030/h.031/h.031_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.032
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.032/h.033
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.032/h.033/h.034
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.032/h.033/h.034/h.035
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Spine0/Spine1/Spine2/Spine3/Neck/Head/h.032/h.033/h.034/h.035/h.035_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.L/cToePivot.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.L/cToePivot.L/footIK.L
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.L/cToePivot.L/footIK.L/footIK.L_end
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.R/cToePivot.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.R/cToePivot.R/footIK.R
    m_Weight: 1
  - m_Path: Armature/cMainV/cMainH/Toe.R/cToePivot.R/footIK.R/footIK.R_end
    m_Weight: 1
  - m_Path: B-CORE
    m_Weight: 1
  - m_Path: B-Eyes
    m_Weight: 1
  - m_Path: B-Face
    m_Weight: 1
  - m_Path: B-foot
    m_Weight: 1
  - m_Path: B-Head
    m_Weight: 1
  - m_Path: B-hip
    m_Weight: 1
  - m_Path: B-IK-elbowL
    m_Weight: 1
  - m_Path: B-IK-elbowR
    m_Weight: 1
  - m_Path: B-IK-hand
    m_Weight: 1
  - m_Path: B-IK-kneeL
    m_Weight: 1
  - m_Path: B-IK-kneeR
    m_Weight: 1
  - m_Path: B-Lower
    m_Weight: 1
  - m_Path: B-MainH
    m_Weight: 1
  - m_Path: B-MainV
    m_Weight: 1
  - m_Path: B-Mouth
    m_Weight: 1
  - m_Path: B-Shrugger
    m_Weight: 1
  - m_Path: B-ToePivoter
    m_Weight: 1
  - m_Path: B-Tongue
    m_Weight: 1
  - m_Path: B-Torso
    m_Weight: 1
  - m_Path: B-Twist
    m_Weight: 1
  - m_Path: B-Upper
    m_Weight: 1
  - m_Path: Blenda-BODY.001
    m_Weight: 1
  - m_Path: Blenda-BODY.001/Staff
    m_Weight: 1
  - m_Path: boob.L_spring
    m_Weight: 1
  - m_Path: boob.R_spring
    m_Weight: 1
  - m_Path: eyeBall.L
    m_Weight: 1
  - m_Path: eyeBall.R
    m_Weight: 1
  - m_Path: Hair
    m_Weight: 1
