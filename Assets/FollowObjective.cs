using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObjective : MonoBehaviour
{
    public GameObject target;
    Vector3 newTarget;
    float targetX;
    float targetY;
    float targetZ;
    public float lerp = 0.2f;
    
    // Update is called once per frame
    void Update()
    {
        targetY = target.transform.position.y;
        targetX = Mathf.Lerp(target.transform.position.x, transform.position.x, lerp);
        targetZ = Mathf.Lerp(target.transform.position.z, transform.position.z, lerp);
        newTarget = new Vector3(targetX, targetY ,targetZ);
        this.transform.position = newTarget;
    }
}
