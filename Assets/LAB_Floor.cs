using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LAB_Floor : MonoBehaviour
{
    public Renderer floorMesh;
    public Material GoodFloorMat;
    public Material BadFloorMat;
    public TriggerInterface OnHitTrigger;

    public List<Enemy> enemies;
    bool enemyFight = false;
    [HideInInspector]
    public bool completed = false;

    private void OnTriggerEnter(Collider collision)
    {
        UpdateTargets();
        if (collision.gameObject.CompareTag("Player") && !enemyFight)
        {
            SwapMaterial();
        }
        if (collision.gameObject.CompareTag("EnemyRange") || collision.gameObject.CompareTag("Enemy"))
        {
            
            Enemy e = collision.gameObject.GetComponent<Enemy>();
            if (e == null)
            {
                return;
            }
            if(e.tipoDeComportamiento == Enemy.Estados.Death)
            {
                return;
            }
            if (enemies.IndexOf(e) != -1)
            {
                return;
            }
            enemies.Add(e);
            floorMesh.material = BadFloorMat;
            enemyFight = true;
            completed = false;

        }
    }
    private void OnTriggerStay(Collider other)
    {
        UpdateTargets();
        if (other.gameObject.CompareTag("EnemyRange") || other.gameObject.CompareTag("Enemy"))
        {
            Enemy e = other.gameObject.GetComponent<Enemy>();
            if (e == null)
            {
                return;
            }
            if (e.tipoDeComportamiento == Enemy.Estados.Death)
            {
                return;
            }
            if (enemies.IndexOf(e) != -1)
            {
                return;
            }
            enemies.Add(e);
            enemyFight = true;
            completed = false;
            floorMesh.material = BadFloorMat;
        }        

        else if (other.gameObject.CompareTag("Player") && !enemyFight)
        {
            SwapMaterial();
            return;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("EnemyRange") || other.gameObject.CompareTag("Enemy"))
        {
            enemies.Remove(other.GetComponent<Enemy>());
        }
    }

    public void UpdateTargets()
    {
        List<Enemy> toRemove = new List<Enemy>();
        foreach (Enemy h in enemies)
        {
            if (
                h != null &&                 
                (
                    h.tipoDeComportamiento == Enemy.Estados.Death ||
                    h.gameObject == null ||
                    h.gameObject != null && !h.gameObject.activeSelf
                )
            )
            {
                toRemove.Add(h);
            }
        }
        foreach (Enemy h in toRemove)
        {
            enemies.Remove(h);
        }
        if (enemies.Count == 0)
        {         
            enemyFight = false;
        }
    }

    void SwapMaterial()
    {
        if (!completed)
        {
            floorMesh.material = GoodFloorMat;
            completed = true;
            if (OnHitTrigger != null)
            {
                OnHitTrigger.Fire();
            }
        }
        else
        {
            return;
        }
    }
}
