using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMaterialTrigger : TriggerInterface
{
    public static string[] TriggerCategories = { "Trigger", "Logic" };
    [Header("Conections")]
    public Renderer target;
    public Material newMaterial;
    public int shaderSlot;

    public override void Cancel()
    { }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        target.materials[shaderSlot] = newMaterial;
    }   

}
