/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CAPSULE = 2236713990U;
        static const AkUniqueID CAPSULEEXPLOSION = 2469166871U;
        static const AkUniqueID CHIEMOVEMENT = 2434147981U;
        static const AkUniqueID MUSIC = 3991942870U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AREA1
        {
            static const AkUniqueID GROUP = 629923843U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID ROBOT1 = 2494382436U;
                static const AkUniqueID ROBOT2 = 2494382439U;
            } // namespace STATE
        } // namespace AREA1

        namespace AREA3
        {
            static const AkUniqueID GROUP = 629923841U;

            namespace STATE
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace AREA3

        namespace HEALTHSADTHEME
        {
            static const AkUniqueID GROUP = 1759481244U;

            namespace STATE
            {
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace HEALTHSADTHEME

        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID A1COMBAT = 4069542215U;
                static const AkUniqueID AREA1 = 629923843U;
                static const AkUniqueID AREA2 = 629923840U;
                static const AkUniqueID AREA3 = 629923841U;
                static const AkUniqueID INITAREA = 1935088176U;
                static const AkUniqueID MAINSCREEN = 3658393046U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSIC

    } // namespace STATES

    namespace SWITCHES
    {
        namespace CHIEMOVEMENT
        {
            static const AkUniqueID GROUP = 2434147981U;

            namespace SWITCH
            {
                static const AkUniqueID ATTACKING = 1641806523U;
                static const AkUniqueID JUMPING = 68157183U;
                static const AkUniqueID WALKING = 340271938U;
            } // namespace SWITCH
        } // namespace CHIEMOVEMENT

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID HEALTH = 3677180323U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MUSICBANK = 3017428748U;
        static const AkUniqueID PLAYERBANK = 1420798232U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
