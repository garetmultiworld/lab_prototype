﻿using UnityEngine;

public class TriggerDebugCheck : TriggerInterface
{

    public string Text;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Debug.Log(Text);
    }

}
