﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnUIClick : MonoBehaviour
{

    public TriggerInterface InsideUI;
    public TriggerInterface OutsideUI;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (InsideUI != null)
                {
                    InsideUI.Fire();
                }
            }
            else
            {
                if (OutsideUI != null)
                {
                    OutsideUI.Fire();
                }
            }
        }
    }

}
