﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSourceImageTrigger : TriggerInterface
{
    public Image image;
    public Sprite sprite;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        image.sprite = sprite;
    }

}
