﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableButtonTrigger : TriggerInterface
{
    public Button button;

    public override void Cancel()
    {
        button.interactable = true;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        button.interactable = false;
    }

}
