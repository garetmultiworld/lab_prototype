﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDialogTrigger : TriggerInterface
{

    public string Dialog;
    public TriggerInterface OnYes;
    public TriggerInterface OnNo;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        ConfirmDialogManager.Instance.ShowConfirm(Dialog, OnYes, OnNo);
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("Yes", OnYes,0);
        callbacks[1] = new TriggerCallback("No", OnNo,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            OnYes = trigger;
        }
        else
        {
            OnNo = trigger;
        }
    }
#endif

}
