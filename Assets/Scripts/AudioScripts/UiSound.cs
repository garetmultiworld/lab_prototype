using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSound : MonoBehaviour
{
    public AK.Wwise.Event clickSound = null;
    public AK.Wwise.Event hoverSound = null;

    public void ClickSound()
    {
        clickSound.Post(gameObject);
    }

    public void HoverSound()
    {
        hoverSound.Post(gameObject);
    }
}
