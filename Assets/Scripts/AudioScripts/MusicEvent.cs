using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicEvent : MonoBehaviour
{
    public static MusicEvent Instance { get; private set; }
    public AK.Wwise.Event playMusic;
    public AK.Wwise.State initialState;
    public AK.Wwise.Bank musicBank;
    private void Awake()
    {   
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        musicBank.Load();
    }

    private void Start()
    {
        initialState.SetValue();
        playMusic.Post(gameObject);
    }
}
