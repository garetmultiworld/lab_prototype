using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundbankManager : MonoBehaviour
{
    public SoundbankItem[] wwiseBanks = new SoundbankItem[0];
    [Tooltip("Wwise Banks name must be Equal to Soundbanks string")]
    public string[] soundbanks = new string[0];
    void Awake()
    {
        /*int index = 0;
        while (index < soundbanks.Length)
        {
            Soundbanks soundbank = soundbanks[index];
            LoadSoundbanks(soundbank);
            index++; //index = index +1;
        }
        // Aqui termina la version dificil. 
        */

        foreach(string soundbank in soundbanks)
        {
            LoadSoundbanks(soundbank);
        }
    }

    public void LoadSoundbanks(string Load)
    {   
       foreach(SoundbankItem wwisebank in wwiseBanks)
        {
            if(wwisebank.name.Equals(Load))
            {
                wwisebank.bank.Load();
            }
        }

    }
}
