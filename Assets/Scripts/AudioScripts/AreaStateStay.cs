using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaStateStay : MonoBehaviour
{
    public AK.Wwise.State combatMusic;
    public AK.Wwise.State exitState;
    public string enemyTag = "Enemy";
    public string enemyRangeTag = "EnemyRange";
    public GameObject[] enemies;
    public GameObject[] rangedEnemies;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == enemyTag)
        {
            combatMusic.SetValue();
        }

        enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        rangedEnemies = GameObject.FindGameObjectsWithTag(enemyRangeTag);

        if (enemies.Length == 0 && rangedEnemies.Length == 0) 
        {
            exitState.SetValue();
        }
        else
        {
            return;
        }
    }
}
