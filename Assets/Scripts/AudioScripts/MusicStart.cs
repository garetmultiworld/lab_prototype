using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStart : MonoBehaviour
{
    public AK.Wwise.State startMusic;
    // Start is called before the first frame update
    void Start()
    {
        startMusic.SetValue();
    }

}
