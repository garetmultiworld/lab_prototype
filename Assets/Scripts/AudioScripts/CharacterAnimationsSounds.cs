using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationsSounds : MonoBehaviour
{
    public AK.Wwise.Event AnimationSounds;
    public string switchGroup = null;
    public enum AnimationSwitch { walking, running, jumping, attacking}
    public AnimationSwitch sound;

    void PlayAnimationSound(AnimationSwitch animationSwitch)
    {
        switch(animationSwitch)
        {
            case AnimationSwitch.walking:
                AkSoundEngine.SetSwitch(switchGroup, "Walking", gameObject);
                break;
            case AnimationSwitch.running:
                AkSoundEngine.SetSwitch(switchGroup, "Running", gameObject);
                break;
            case AnimationSwitch.jumping:
                AkSoundEngine.SetSwitch(switchGroup, "Jumping", gameObject);
                print("Jumping");
                break;
            case AnimationSwitch.attacking:
                AkSoundEngine.SetSwitch(switchGroup, "Attacking", gameObject);
                break;
        }
        AnimationSounds.Post(gameObject);
    }
}
