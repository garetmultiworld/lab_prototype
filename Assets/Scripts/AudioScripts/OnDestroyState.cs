using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroyState : MonoBehaviour
{
    public AK.Wwise.State EnemyDestroySate;
    private void OnDestroy()
    {
        EnemyDestroySate.SetValue();
    }
}
