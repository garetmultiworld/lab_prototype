using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicAreaSwitch : MonoBehaviour
{
    public AK.Wwise.State enterState;
    public AK.Wwise.State exitState;

    private void OnTriggerEnter(Collider collider)
    {   
        if( collider.gameObject.tag == "Player")
        {
            enterState.SetValue();
        }
    }

    private void OnTriggerExit(Collider collider)
    {   
        if( collider.gameObject.tag == "Player")
        {
            print("chao");
        }
    }
}
