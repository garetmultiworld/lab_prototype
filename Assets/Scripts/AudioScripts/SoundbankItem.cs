using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundbankItem
{
    public string name;
    public AK.Wwise.Bank bank;
}
