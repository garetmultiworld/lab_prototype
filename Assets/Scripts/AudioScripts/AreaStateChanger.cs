using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaStateChanger : MonoBehaviour
{
    public string playerTag = null;
    public AK.Wwise.State onEnterState;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == playerTag)
        {
            onEnterState.SetValue();
        }
    }
}
