using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStateChanger : MonoBehaviour
{
    public AK.Wwise.State onSceneStartState;
    void Start()
    {
        onSceneStartState.SetValue();
    }
}
