﻿public class SetSavedIntTrigger : SetSavedDataTrigger
{
    public int Value;

    public override void SetSavedData()
    {
        PersistanceManager.Instance.SetSavedData(DataName, Value);
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Value + ")";
        return desc;
    }

}
