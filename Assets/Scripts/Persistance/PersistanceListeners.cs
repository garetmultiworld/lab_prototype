﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PersistanceListeners
{
    public string DataName;
    public List<PersistanceListener> listeners = new List<PersistanceListener>();
    
    public void TriggerEvent(PersistanceManager.EventType eventType, string DataName = "", PersistanceManager.DataType dataType = PersistanceManager.DataType.Bool)
    {
        foreach (PersistanceListener listener in listeners)
        {
            if (listener != null)
            {
                listener.TriggerEvent(eventType, DataName, dataType);
            }
        }
    }
}
