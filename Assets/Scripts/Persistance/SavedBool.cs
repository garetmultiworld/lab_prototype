﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SavedBool
{
    public string Name;
    public bool Value;
}
