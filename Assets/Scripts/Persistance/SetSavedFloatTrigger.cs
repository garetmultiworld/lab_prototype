﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSavedFloatTrigger : SetSavedDataTrigger
{
    public float Value;

    public override void SetSavedData()
    {
        PersistanceManager.Instance.SetSavedData(DataName, Value);
    }
}
