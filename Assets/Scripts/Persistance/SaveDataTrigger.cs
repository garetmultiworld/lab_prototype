﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataTrigger : TriggerInterface
{

    public static string[] TriggerCategories = { "Trigger", "Persistance" };

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.SaveData();
    }

}
