﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SavedFloat
{
    public string Name;
    public float Value;
}
