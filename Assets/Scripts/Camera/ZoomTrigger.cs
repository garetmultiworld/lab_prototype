﻿using Cinemachine;
using UnityEngine;

public class ZoomTrigger : TriggerInterface
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;
    public bool Animate=true;
    public float Speed;
    public float TargetZoom;
    public AnimationCurve AnimCurve;
    public TriggerInterface TriggerOnComplete;

    private bool IsAnimating=false;
    private float StartingZoom;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (!Animate)
        {
            cinemachineVirtualCamera.m_Lens.OrthographicSize = TargetZoom;
        }
        else
        {
            StartingZoom = cinemachineVirtualCamera.m_Lens.OrthographicSize;
            IsAnimating = true;
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        if (
            (cinemachineVirtualCamera.m_Lens.OrthographicSize - 0.1f) <= TargetZoom &&
            (cinemachineVirtualCamera.m_Lens.OrthographicSize + 0.1f) >= TargetZoom
        )
        {
            cinemachineVirtualCamera.m_Lens.OrthographicSize = TargetZoom;
            IsAnimating = false;
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Fire(this);
            }
        }
        else
        {
            float timeLapse;
            if (TargetZoom > cinemachineVirtualCamera.m_Lens.OrthographicSize)
            {
                if (TargetZoom <= 0.01 && TargetZoom >= -0.01)
                {
                    timeLapse = cinemachineVirtualCamera.m_Lens.OrthographicSize / 0.01f;
                }
                else
                {
                    timeLapse = cinemachineVirtualCamera.m_Lens.OrthographicSize / TargetZoom;
                }
                cinemachineVirtualCamera.m_Lens.OrthographicSize += Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                if (cinemachineVirtualCamera.m_Lens.OrthographicSize <= 0.01 && cinemachineVirtualCamera.m_Lens.OrthographicSize >= -0.01)
                {
                    timeLapse = TargetZoom / 0.01f;
                }
                else
                {
                    timeLapse = TargetZoom / cinemachineVirtualCamera.m_Lens.OrthographicSize;
                }
                cinemachineVirtualCamera.m_Lens.OrthographicSize -= Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[0];
        callbacks[0] = new TriggerCallback("On Complete", TriggerOnComplete, 0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerOnComplete = trigger;
    }
#endif

}
