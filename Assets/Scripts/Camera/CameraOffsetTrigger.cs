﻿using UnityEngine;

public class CameraOffsetTrigger : TriggerInterface
{

    public CinemachineCameraOffset cinemachineCameraOffset;
    public Vector3 Offset;
    public bool Animate;
    public float Speed;
    public TriggerInterface TriggerOnComplete;
    public AnimationCurve AnimCurve;

    private bool IsAnimating = false;
    private bool NewIsGreaterX;
    private bool NewIsGreaterY;
    private bool NewIsGreaterZ;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (!Animate)
        {
            cinemachineCameraOffset.m_Offset = new Vector3(Offset.x,Offset.y,Offset.z);
        }
        else
        {
            NewIsGreaterX = Offset.x > cinemachineCameraOffset.m_Offset.x;
            NewIsGreaterY = Offset.y > cinemachineCameraOffset.m_Offset.y;
            NewIsGreaterZ = Offset.z > cinemachineCameraOffset.m_Offset.z;
            IsAnimating = true;
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
    }
        float timeLapse;
        int complete = 0;
        if (NewIsGreaterX)
        {
            if (Offset.x > cinemachineCameraOffset.m_Offset.x)
            {
                if (Offset.x <= 0.01 && Offset.x >= -0.01)
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.x / 0.01f;
                }
                else
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.x / Offset.x;
                }
                cinemachineCameraOffset.m_Offset.x += Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.x = Offset.x;
                complete++;
            }
        }
        else
        {
            if (Offset.x < cinemachineCameraOffset.m_Offset.x)
            {
                if (cinemachineCameraOffset.m_Offset.x <= 0.01 && cinemachineCameraOffset.m_Offset.x >= -0.01)
                {
                    timeLapse = Offset.x / 0.01f;
                }
                else
                {
                    timeLapse = Offset.x / cinemachineCameraOffset.m_Offset.x;
                }
                cinemachineCameraOffset.m_Offset.x -= Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.x = Offset.x;
                complete++;
            }
        }
        if (NewIsGreaterY)
        {
            if (Offset.y > cinemachineCameraOffset.m_Offset.y)
            {
                if (Offset.y <= 0.01 && Offset.y >= -0.01)
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.y / 0.01f;
                }
                else
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.y / Offset.y;
                }
                cinemachineCameraOffset.m_Offset.y += Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.y = Offset.y;
                complete++;
            }
        }
        else
        {
            if (Offset.y < cinemachineCameraOffset.m_Offset.y)
            {
                if (cinemachineCameraOffset.m_Offset.y <= 0.01 && cinemachineCameraOffset.m_Offset.y >= -0.01)
                {
                    timeLapse = Offset.y / 0.01f;
                }
                else
                {
                    timeLapse = Offset.y / cinemachineCameraOffset.m_Offset.y;
                }
                cinemachineCameraOffset.m_Offset.y -= Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.y = Offset.y;
                complete++;
            }
        }
        if (NewIsGreaterZ)
        {
            if (Offset.z > cinemachineCameraOffset.m_Offset.z)
            {
                if (Offset.z <= 0.01 && Offset.z >= -0.01)
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.z / 0.01f;
                }
                else
                {
                    timeLapse = cinemachineCameraOffset.m_Offset.z / Offset.z;
                }
                cinemachineCameraOffset.m_Offset.z += Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.z = Offset.z;
                complete++;
            }
        }
        else
        {
            if (Offset.z < cinemachineCameraOffset.m_Offset.z)
            {
                if (cinemachineCameraOffset.m_Offset.z <= 0.01 && cinemachineCameraOffset.m_Offset.z >= -0.01)
                {
                    timeLapse = Offset.z / 0.01f;
                }
                else
                {
                    timeLapse = Offset.z / cinemachineCameraOffset.m_Offset.z;
                }
                cinemachineCameraOffset.m_Offset.z -= Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                cinemachineCameraOffset.m_Offset.z = Offset.z;
                complete++;
            }
        }
        if (complete == 3)
        {
            IsAnimating = false;
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Fire(this);
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Complete", TriggerOnComplete,0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerOnComplete = trigger;
    }
#endif

}
