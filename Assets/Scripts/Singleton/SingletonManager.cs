﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mw.singleton
{
    public class SingletonManager:MonoBehaviour
    {

        #region Static Instance
        private static SingletonManager instance;
        public static SingletonManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<SingletonManager>();
                    if (instance == null)
                    {
                        instance = new GameObject("Spawned SingletonManager", typeof(SingletonManager)).GetComponent<SingletonManager>();
                        DontDestroyOnLoad(instance);
                    }
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }
        #endregion

        public string[] SceneLoadExceptions;
        public Inventory[] inventories;

        private static bool initialized = false;

        public static void InitSingletons()
        {
            if (initialized)
            {
                return;
            }
            initialized = true;
            InterSceneManager interSceneManager = InterSceneManager.Instance;
            interSceneManager.SceneLoadExceptions = Instance.SceneLoadExceptions;
            Debug.Log(InterSceneManager.Instance.SceneLoadExceptions);
            PersistanceManager persistance = PersistanceManager.Instance;
            persistance.inventories = Instance.inventories;
            _ = InventoryManager.Instance;
            _ = PlayerManager.Instance;
        }

        void Awake()
        {
            InitSingletons();
        }

    }
}