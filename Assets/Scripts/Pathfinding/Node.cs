﻿using UnityEngine;

namespace mw.Pathfinding
{
    public class Node
    {
        public bool valid = true;
        public Vector2 position;
        public readonly Node[] connections;
        public float gCost;
        public float hCost;
        public float fCost { get { return gCost + hCost; } }
        public Node parent;
        public GameObject associatedObject;

        public Node(Vector2 pos)
        {
            position = pos;
            connections = new Node[8];
        }

        public void Reset()
        {
            valid = true;
            associatedObject = null;
            gCost = 0;
            hCost = 0;
            parent = null;
        }

        public bool AnyConnectionsBad()
        {
            for (int i = 0; i < connections.Length; i++)
            {
                if (connections[i] == null || !connections[i].valid)
                {
                    return true;
                }
            }
            return false;
        }
    }
}