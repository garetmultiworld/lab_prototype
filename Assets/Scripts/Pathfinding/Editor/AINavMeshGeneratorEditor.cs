﻿using mw.Pathfinding;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AINavMeshGenerator))]
public class AINavMeshGeneratorEditor : Editor
{
    void OnSceneGUI()
    {
        AINavMeshGenerator source = target as AINavMeshGenerator;

        Rect rect = RectUtils.ResizeRect(source.size,
            Handles.CubeHandleCap,
            Color.green, new Color(1, 1, 1, 0.5f),
            HandleUtility.GetHandleSize(Vector3.zero) * 0.1f,
            0.1f);

        source.size = rect;
    }

    public override void OnInspectorGUI()
    {
        AINavMeshGenerator source = target as AINavMeshGenerator;
        base.OnInspectorGUI();
        if (GUILayout.Button("Generate grid"))
        {
            source.GenerateNewGrid();
        }
        if (GUILayout.Button("Clear grid"))
        {
            source.ClearGrid();
        }
    }

}
