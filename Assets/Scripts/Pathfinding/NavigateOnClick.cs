﻿using mw.Pathfinding;
using mw.player;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class NavigateOnClick : MonoBehaviour
{

    public Character player;
    public AINavMeshGenerator navMesh;
    public float cooldown = 0.2f;

    private Vector3 _last_click;
    private Vector2[] _last_path;

    public Vector3 Last_click { get => _last_click; }
    public Vector2[] Last_path { get => _last_path; }

    private bool clickMade = false;
    private Vector3 mouseFinalPos;
    private float lastCount=0;

    void Update()
    {
        if (lastCount < cooldown)
        {
            lastCount += Time.deltaTime;
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                    return;
            }
            clickMade = true;
        }else if (clickMade && Input.GetMouseButtonUp(0))
        {
            lastCount = 0;
            clickMade = false;
            mouseFinalPos = Input.mousePosition;
            //if (Vector3.Distance(mouseInitialPos, mouseFinalPos) < 0.1f)//was a click, not a drag
            {
                _last_click = Camera.main.ScreenToWorldPoint(mouseFinalPos);
                if (navMesh.IsWithinBounds(_last_click))
                {
                    navMesh.CheckForBadNodes();
                    _last_path = navMesh.pathfinder.FindPath(player.transform.position, _last_click, null);
                    if (_last_path != null)
                    {
                        if (_last_path.Length > 1)
                        {
                            _last_path = _last_path.Skip(1).ToArray();
                        }
                        player.Stimuli("NavigateOnClick", gameObject);
                    }
                }
            }
        }
    }

    public void RecreateStimuli()
    {
        if (_last_path == null)
        {
            return;
        }
        player.Stimuli("NavigateOnClick", gameObject);
    }
}
