﻿using mw.Pathfinding;
using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigateTrigger : TriggerInterface
{

    public Transform[] Destinations=new Transform[0];
    public Character player;
    public AINavMeshGenerator navMesh;
    public TriggerInterface OnNavigationEnd;
    public TriggerInterface OnArrive;
    public TriggerInterface OnNextDestination;

    private Vector3 _last_position;
    private Vector2[] _last_path;
    private int currentIndex=-1;

    public Vector3 Last_position { get => _last_position; }
    public Vector2[] Last_path { get => _last_path; }

    public override void Cancel()
    {
        player.Stimuli("StopNavigate", gameObject);
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        currentIndex = 0;
        GoToDestination();
        player.Stimuli("Navigate", gameObject);
    }

    public bool HasAnotherDestination()
    {
        currentIndex++;
        return (currentIndex < Destinations.Length);
    }

    public void GoToDestination()
    {
        _last_position = Destinations[currentIndex].position;
        if (navMesh.IsWithinBounds(_last_position))
        {
            navMesh.GenerateNewGrid();
            navMesh.CheckForBadNodes();
            _last_path = navMesh.pathfinder.FindPath(player.transform.position, _last_position, null);
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[3];
        callbacks[0] = new TriggerCallback("On Next Destination", OnNextDestination,0);
        callbacks[1] = new TriggerCallback("On Arrive", OnArrive,1);
        callbacks[2] = new TriggerCallback("On Navigation End", OnNavigationEnd,2);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        switch (index)
        {
            case 0:
                OnNextDestination = trigger;
                break;
            case 1:
                OnArrive = trigger;
                break;
            case 2:
                OnNavigationEnd = trigger;
                break;
        }
    }
#endif

}
