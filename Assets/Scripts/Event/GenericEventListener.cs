﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericEventListener : MonoBehaviour, TriggerInvoker
{

#if UNITY_EDITOR
    [HideInInspector]
    public Vector2 nodeEditorPos;
#endif

    public static string[] TriggerCategories = { "Trigger Invoker", "Event" };

    public string EventName;
    public TriggerInterface OnEvent;

    private bool _registered = false;

    void Awake()
    {
        Register();
    }

    public void Register()
    {
        if (_registered|| string.IsNullOrEmpty(EventName))
        {
            return;
        }
        _registered = GenericEventManager.Instance.RegisterListener(this);
    }

    public void TriggerStringEvent(GameObject source, string EventParameter, float EventNumber, int EventInteger)
    {
        if (!gameObject.activeSelf)
        {
            return;
        }
        if (OnEvent!= null)
        {
            OnEvent.StringParameter = EventParameter;
            OnEvent.FloatParameter = EventNumber;
            OnEvent.IntParameter = EventInteger;
            OnEvent.EventSource = source;
            OnEvent.Fire();
        }
    }

#if UNITY_EDITOR
    public TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Event", OnEvent, 0);
        return callbacks;
    }

    public void SetCallback(int index, TriggerInterface trigger)
    {
        OnEvent = trigger;
    }

    public virtual Vector2 GetNodeEditorPos()
    {
        return nodeEditorPos;
    }

    public virtual void SetNodeEditorPos(Vector2 pos)
    {
        nodeEditorPos = pos;
    }
#endif

}
