﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(GenericEventListener))]
public class GenericEventListenerEditor : EditorBase
{

    protected void SetUpPrefabConflict(GenericEventListener genericEventListener)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(genericEventListener, "GenericEventListener");
    }

    protected void StorePrefabConflict(GenericEventListener genericEventListener)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(genericEventListener);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(genericEventListener.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        GenericEventListener genericEventListener = (GenericEventListener)target;
        SetUpPrefabConflict(genericEventListener);
        genericEventListener.EventName = EditorUtils.TextField(this,"Event Name", genericEventListener.EventName);
        genericEventListener.OnEvent = EditorUtils.SelectTrigger(this,"On Event", genericEventListener.OnEvent);
        StorePrefabConflict(genericEventListener);
    }
}
