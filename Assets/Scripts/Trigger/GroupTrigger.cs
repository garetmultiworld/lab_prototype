﻿public class GroupTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showGroupTrigger=true;
#endif

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public TriggerInterface[] Triggers= { };

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.FloatParameter = FloatParameter;
                trigger.Fire(this);
            }
        }
    }

    public override void Cancel()
    {
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.Cancel();
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Triggers.Length.ToString() + " elements, ";
        int assigned = 0;
        foreach(TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                assigned++;
            }
        }
        desc += assigned.ToString()+" assigned)";
        return desc;
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[Triggers.Length];
        for (int i = 0; i < Triggers.Length; i++)
        {
            callbacks[i] = new TriggerCallback(i.ToString(), Triggers[i], i);
        }
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        Triggers[index] = trigger;
    }
#endif

}
