﻿using System.Collections.Generic;
using UnityEngine;

public class TriggerIfWithinArea : TriggerInterface
{

    public LayerMask FilterLayer;
    public int Quantity = 1;
    public TriggerInterface TriggerIfWithin;
    public TriggerInterface TriggerIfNotWithin;
    public bool CheckOnEnterAndExit=true;

    private List<Collider> colliders = new List<Collider>();

    public void OnTriggerEnter(Collider c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            if (!colliders.Contains(c))
            {
                colliders.Add(c);
            }
        }
    }

    public void OnTriggerStay(Collider c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            if (!colliders.Contains(c))
            {
                colliders.Add(c);
            }
        }
    }

    public void OnTriggerExit(Collider c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            colliders.Remove(c);
        }
    }

    private void UpdateColliders()
    {
        List<int> toRemove = new List<int>();
        for(int i = 0; i < colliders.Count; i++)
        {
            Collider c = colliders[i];
            if (
                c==null||
                (
                    c != null &&
                    (
                        c.gameObject == null || !c.gameObject.activeSelf
                    )
                )
            )
            {
                toRemove.Add(i);
            }
        }
        foreach(int index in toRemove)
        {
            colliders.RemoveAt(index);
        }
        colliders.TrimExcess();
    }

    public override void Cancel()
    {
        UpdateColliders();
        if (colliders.Count>=Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Cancel();
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Cancel();
            }
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        UpdateColliders();
        if (colliders.Count >= Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Fire(this);
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Fire(this);
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("If Within", TriggerIfWithin,0);
        callbacks[1] = new TriggerCallback("If Not Within", TriggerIfNotWithin,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            TriggerIfWithin = trigger;
        }
        else
        {
            TriggerIfNotWithin = trigger;
        }
    }
#endif

}
