﻿using UnityEngine;

public abstract class TriggerInterface : MonoBehaviour, TriggerInvoker
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showTriggerInterface;
    [HideInInspector]
    public Vector2 nodeEditorPos;
#endif

    [Tooltip("If this trigger is disabled it won't fire")]
    public bool IsDisabled;
    [Tooltip("The name of this trigger, doesn't have to be unique")]
    public new string name;
    [Tooltip("The maximum amount of times this trigger can be activated, 0 or negative for unlimited")]
    public int MaxActivationTimes=0;

    [HideInInspector]
    public GameObject EventSource = null;
    [HideInInspector]
    public string StringParameter="";
    [HideInInspector]
    public float FloatParameter = 0;
    [HideInInspector]
    public int IntParameter = 0;

    protected int ActivationTimes = 0;

    public bool IgnoreDisabled = false;

    protected bool CanTrigger()
    {
        if (
            IsDisabled ||
            (MaxActivationTimes > 0 && ActivationTimes >= MaxActivationTimes) || 
            (
                !IgnoreDisabled&&
                (!enabled || !gameObject.activeSelf)
            )
        )
        {
            return false;
        }
        ActivationTimes++;
        return true;
    }

    public void Fire(TriggerInterface trigger)
    {
        EventSource = trigger.EventSource;
        StringParameter = trigger.StringParameter;
        FloatParameter = trigger.FloatParameter;
        IntParameter = trigger.IntParameter;
        Fire();
    }

    abstract public void Fire();

    abstract public void Cancel();

#if UNITY_EDITOR
    public virtual TriggerCallback[] GetCallbacks()
    {
        return null;
    }

    public virtual void SetCallback(int index, TriggerInterface trigger)
    {
        
    }

    public virtual Vector2 GetNodeEditorPos()
    {
        return nodeEditorPos;
    }

    public virtual void SetNodeEditorPos(Vector2 pos)
    {
        nodeEditorPos = pos;
    }
#endif

    public string GetLabel()
    {
        string lab = name;
        if (IsDisabled)
        {
            lab += ", disabled";
        }
        if (MaxActivationTimes>0)
        {
            lab += ", max "+ MaxActivationTimes.ToString() + " times";
        }
        return lab;
    }

    virtual public string GetDescription()
    {
        return GetLabel();
    }
    
}
