﻿using UnityEngine;
using System.Collections.Generic;

public class TriggerIfWithinArea2D : TriggerInterface
{

    public LayerMask FilterLayer;
    public int Quantity = 1;
    public TriggerInterface TriggerIfWithin;
    public TriggerInterface TriggerIfNotWithin;
    public bool CheckOnEnterAndExit = true;

    private HashSet<Collider2D> colliders = new HashSet<Collider2D>();

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            colliders.Add(c);
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            colliders.Remove(c);
        }
    }

    private void UpdateColliders()
    {
        List<Collider2D> toRemove = new List<Collider2D>();
        foreach (Collider2D c in colliders)
        {
            if (
                c != null &&
                (
                    c.gameObject == null ||
                    c.gameObject != null && !c.gameObject.activeSelf
                )
            )
            {
                toRemove.Add(c);
            }
        }
        foreach (Collider2D c in toRemove)
        {
            colliders.Remove(c);
        }
        colliders.TrimExcess();
    }

    public override void Cancel()
    {
        UpdateColliders();
        if (colliders.Count >= Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Cancel();
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Cancel();
            }
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        UpdateColliders();
        if (colliders.Count >= Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Fire(this);
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Fire(this);
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("If Within", TriggerIfWithin,0);
        callbacks[1] = new TriggerCallback("If Not Within", TriggerIfNotWithin,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            TriggerIfWithin = trigger;
        }
        else
        {
            TriggerIfNotWithin = trigger;
        }
    }
#endif

}
