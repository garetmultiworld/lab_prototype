﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeTrigger : TriggerInterface
{

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public delegate void DelegateTrigger();

    public DelegateTrigger handlerCancel;
    public DelegateTrigger handlerFire;

    public override void Cancel()
    {
        if (handlerCancel != null)
        {
            handlerCancel();
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (handlerFire != null)
        {
            handlerFire();
        }
    }

}
