﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour, TriggerInvoker
{
#if UNITY_EDITOR
    [HideInInspector]
    public Vector2 nodeEditorPos;
#endif

    public static string[] TriggerCategories = { "Trigger Invoker", "Physics/Collider" };

    [Tooltip("The trigger fired when the character enters")]
    public TriggerInterface OnEnter;
    [Tooltip("If the enter and exit trigger can't exist at the same time")]
    public bool EnterAndExitExclusive = false;
    [Tooltip("The trigger fired when the character exits")]
    public TriggerInterface OnExit;
    public LayerMask FilterLayer;
    public TargetHolder TargetOnEnter;
    public TargetHolder TargetOnExit;

    public void OnTriggerEnter(Collider c)
    {
        if (OnEnter != null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            if (TargetOnEnter != null)
            {
                TargetOnEnter.Target = c.gameObject;
            }
            if (EnterAndExitExclusive)
            {
                OnExit.Cancel();
            }
            OnEnter.Fire();
        }
    }

    public void OnTriggerExit(Collider c)
    {
        if (OnExit != null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            if (TargetOnExit != null)
            {
                TargetOnExit.Target = c.gameObject;
            }
            if (EnterAndExitExclusive)
            {
                OnEnter.Cancel();
            }
            OnExit.Fire();
        }
    }

#if UNITY_EDITOR
    public TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("On Enter", OnEnter, 0);
        callbacks[1] = new TriggerCallback("On Exit", OnExit, 1);
        return callbacks;
    }

    public void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            OnEnter = trigger;
        }
        else
        {
            OnExit = trigger;
        }
    }

    public virtual Vector2 GetNodeEditorPos()
    {
        return nodeEditorPos;
    }

    public virtual void SetNodeEditorPos(Vector2 pos)
    {
        nodeEditorPos = pos;
    }
#endif

}
