﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

public class TriggerWindowEditor : EditorWindow
{
    private List<TriggerEditorNode> nodes=new List<TriggerEditorNode>();
    private List<TriggerEditorConnection> connections=new List<TriggerEditorConnection>();

    private GUIStyle nodeStyle;
    private GUIStyle selectedNodeStyle;
    private GUIStyle inPointStyle;
    private GUIStyle outPointStyle;

    private TriggerEditorConnectionPoint selectedInPoint;
    private TriggerEditorConnectionPoint selectedOutPoint;

    private Vector2 offset;
    private Vector2 drag;

    private IEnumerable<Type> types;

    private List<string> categories=new List<string>();
    private Dictionary<string,List<Type>> categoryDictionary=new Dictionary<string, List<Type>>();
    private List<Type> uncategorized=new List<Type>();

    [MenuItem("Window/Trigger Editor")]
    private static void OpenWindow()
    {
        TriggerWindowEditor window = GetWindow<TriggerWindowEditor>();
        window.titleContent = new GUIContent("Trigger Editor");
    }

    void OnSelectionChange()
    {
        RefreshNodes();
        Repaint();
    }

    void OnFocus()
    {
        types = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(x => x.GetTypes())
                        .Where(x => x.IsClass && typeof(TriggerInvoker).IsAssignableFrom(x))
                        .OrderBy(t => t.Name);
        categories.Clear();
        categoryDictionary.Clear();
        uncategorized.Clear();
        foreach (Type triggerType in types)
        {
            FieldInfo fieldInfo = triggerType.GetField("TriggerCategories");
            if (fieldInfo != null)
            {
                string[] triggerCategories = fieldInfo.GetValue(null) as string[];
                foreach(string category in triggerCategories)
                {
                    if (!categories.Contains(category))
                    {
                        categories.Add(category);
                        categoryDictionary.Add(category, new List<Type>());
                    }
                    categoryDictionary[category].Add(triggerType);
                }
            }
            else
            {
                uncategorized.Add(triggerType);
            }
        }
        categories.Sort();
        RefreshNodes();
    }

    private void OnEnable()
    {
        nodeStyle = new GUIStyle();
        nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
        nodeStyle.border = new RectOffset(12, 12, 12, 12);

        selectedNodeStyle = new GUIStyle();
        selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
        selectedNodeStyle.border = new RectOffset(12, 12, 12, 12);

        inPointStyle = new GUIStyle();
        inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
        inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
        inPointStyle.border = new RectOffset(4, 4, 12, 12);

        outPointStyle = new GUIStyle();
        outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
        outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
        outPointStyle.border = new RectOffset(4, 4, 12, 12);

        RefreshNodes();
    }

    void RefreshNodes()
    {
        connections.Clear();
        nodes.Clear();
        if (Selection.activeGameObject == null)
        {
            return;
        }
        TriggerInvoker[] triggerInvokers = Selection.activeGameObject.GetComponents<TriggerInvoker>();
        foreach(TriggerInvoker triggerInvoker in triggerInvokers)
        {
            TriggerInterface nodeTrigger = null;
            if(triggerInvoker is TriggerInterface)
            {
                nodeTrigger = triggerInvoker as TriggerInterface;
            }
            nodes.Add(new TriggerEditorNode(
                nodeTrigger,
                triggerInvoker,
                nodeStyle,
                selectedNodeStyle,
                inPointStyle,
                outPointStyle,
                OnClickInPoint,
                OnClickOutPoint,
                OnClickRemoveNode));
        }
        foreach (TriggerEditorNode outNode in nodes)
        {
            foreach(TriggerEditorConnectionPoint outPoint in outNode.outPoints)
            {
                if (outPoint.callback.Trigger == null)
                {
                    continue;
                }
                foreach (TriggerEditorNode inNode in nodes)
                {
                    if (outNode == inNode)
                    {
                        continue;
                    }
                    if (outPoint.callback.Trigger == inNode.trigger)
                    {
                        connections.Add(new TriggerEditorConnection(inNode.inPoint, outPoint, OnClickRemoveConnection));
                        break;
                    }
                }
            }
        }
    }

    private void OnGUI()
    {
        DrawGrid(20, 0.2f, Color.gray);
        DrawGrid(100, 0.4f, Color.gray);

        DrawNodes();
        DrawConnections();

        DrawConnectionLine(Event.current);

        ProcessNodeEvents(Event.current);
        ProcessEvents(Event.current);

        if (GUI.changed) Repaint();
    }

    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for (int i = 0; i < widthDivs; i++)
        {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for (int j = 0; j < heightDivs; j++)
        {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawNodes()
    {
        if (nodes != null)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i].Draw();
            }
        }
    }

    private void DrawConnections()
    {
        if (connections != null)
        {
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i].Draw();
            }
        }
    }

    private void ProcessEvents(Event e)
    {
        drag = Vector2.zero;

        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    ClearConnectionSelection();
                }

                if (e.button == 1)
                {
                    ProcessContextMenu(e.mousePosition);
                }
                break;

            case EventType.MouseDrag:
                if (e.button == 0)
                {
                    OnDrag(e.delta);
                }
                break;
        }
    }

    private void ProcessNodeEvents(Event e)
    {
        if (nodes != null)
        {
            for (int i = nodes.Count - 1; i >= 0; i--)
            {
                bool guiChanged = nodes[i].ProcessEvents(e);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    private void DrawConnectionLine(Event e)
    {
        if (selectedInPoint != null && selectedOutPoint == null)
        {
            Handles.DrawBezier(
                selectedInPoint.rect.center,
                e.mousePosition,
                selectedInPoint.rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if (selectedOutPoint != null && selectedInPoint == null)
        {
            Handles.DrawBezier(
                selectedOutPoint.rect.center,
                e.mousePosition,
                selectedOutPoint.rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }

    private void ProcessContextMenu(Vector2 mousePosition)
    {
        GenericMenu genericMenu = new GenericMenu();
        string menuName;
        foreach(string category in categories)
        {
            menuName = category;
            foreach(Type triggerType in categoryDictionary[category])
            {
                genericMenu.AddItem(new GUIContent(category+"/"+triggerType.Name), false, () => OnClickAddNode(mousePosition, triggerType));
            }
        }
        foreach (Type triggerType in uncategorized)
        {
            genericMenu.AddItem(new GUIContent(triggerType.Name), false, () => OnClickAddNode(mousePosition,triggerType));
        }
        genericMenu.ShowAsContext();
    }

    private void OnDrag(Vector2 delta)
    {
        drag = delta;

        if (nodes != null)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private void OnClickAddNode(Vector2 mousePosition,Type nodeType)
    {
        if (nodes == null)
        {
            nodes = new List<TriggerEditorNode>();
        }
        TriggerInvoker nodeInvoker = Selection.activeGameObject.AddComponent(nodeType) as TriggerInvoker;
        TriggerInterface nodeTrigger=null;
        if(nodeInvoker is TriggerInterface)
        {
            nodeTrigger = nodeInvoker as TriggerInterface;
        }
        nodes.Add(new TriggerEditorNode(nodeTrigger, nodeInvoker, nodeStyle, selectedNodeStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode));
    }

    private void OnClickInPoint(TriggerEditorConnectionPoint inPoint)
    {
        selectedInPoint = inPoint;

        if (selectedOutPoint != null)
        {
            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickOutPoint(TriggerEditorConnectionPoint outPoint)
    {
        selectedOutPoint = outPoint;

        if (selectedInPoint != null)
        {
            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickRemoveNode(TriggerEditorNode node)
    {
        if (connections != null)
        {
            List<TriggerEditorConnection> connectionsToRemove = new List<TriggerEditorConnection>();

            for (int i = 0; i < connections.Count; i++)
            {
                if (connections[i].inPoint == node.inPoint ||  node.HasOutPoint(connections[i].outPoint))
                {
                    connectionsToRemove.Add(connections[i]);
                }
            }

            for (int i = 0; i < connectionsToRemove.Count; i++)
            {
                connectionsToRemove[i].outPoint.node.invoker.SetCallback(connectionsToRemove[i].outPoint.callback.Index, null);
                connections.Remove(connectionsToRemove[i]);
            }

            connectionsToRemove = null;
        }
        DestroyImmediate(node.invoker as MonoBehaviour);
        nodes.Remove(node);
    }

    private void OnClickRemoveConnection(TriggerEditorConnection connection)
    {
        connection.outPoint.node.invoker.SetCallback(connection.outPoint.callback.Index,null);
        connections.Remove(connection);
    }

    private TriggerEditorConnection GetOutPointConnection(TriggerEditorConnectionPoint outPoint)
    {
        foreach (TriggerEditorConnection connection in connections)
        {
            if (connection.outPoint == outPoint)
            {
                return connection;
            }
        }
        return null;
    }

    private void CreateConnection()
    {
        if (connections == null)
        {
            connections = new List<TriggerEditorConnection>();
        }
        TriggerEditorConnection connection = GetOutPointConnection(selectedOutPoint);
        connections.Remove(connection);
        connection = new TriggerEditorConnection(selectedInPoint, selectedOutPoint, OnClickRemoveConnection);
        connections.Add(connection);
        connection.outPoint.node.invoker.SetCallback(connection.outPoint.callback.Index, selectedInPoint.node.trigger);
    }

    private void ClearConnectionSelection()
    {
        selectedInPoint = null;
        selectedOutPoint = null;
    }
}