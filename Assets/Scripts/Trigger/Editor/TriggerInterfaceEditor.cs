﻿using UnityEditor;
using UnityEngine;

public class TriggerInterfaceEditor : EditorBase
{

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        TriggerInterface triggerInterface = (TriggerInterface)target;
        triggerInterface.showTriggerInterface = EditorGUILayout.Foldout(
            triggerInterface.showTriggerInterface, 
            "General ("+triggerInterface.GetLabel()+")"
        );
        if (triggerInterface.showTriggerInterface)
        {
            triggerInterface.IsDisabled = EditorUtils.Checkbox(this,"Is Disabled", triggerInterface.IsDisabled);

            triggerInterface.IgnoreDisabled = EditorUtils.Checkbox(this,"Allow fire even if everyting is disabled", triggerInterface.IgnoreDisabled);

            triggerInterface.name= EditorUtils.TextField(this,"Name", triggerInterface.name);

            triggerInterface.MaxActivationTimes= EditorUtils.IntField(this,
                new GUIContent(
                    "Max Activation Times",
                    "The maximum amount of times this trigger can be activated, 0 or negative for unlimited"
                ),
                triggerInterface.MaxActivationTimes
            );
        }
    }
}
