﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DelayedTrigger), true)]
public class DelayedTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(DelayedTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "DelayedTrigger");
    }

    protected void StorePrefabConflict(DelayedTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DelayedTrigger delayedTrigger = (DelayedTrigger)target;
        SetUpPrefabConflict(delayedTrigger);
        delayedTrigger.showDelayedTrigger = EditorGUILayout.Foldout(
            delayedTrigger.showDelayedTrigger,
            "Delayed"
        );
        if (delayedTrigger.showDelayedTrigger)
        {
            delayedTrigger.TriggerToFire = EditorUtils.SelectTrigger(this,"Trigger To Fire", delayedTrigger.TriggerToFire);
            delayedTrigger.Interval = EditorUtils.FloatField(this,new GUIContent("Interval", "The time before the trigger is fired (in seconds)."), delayedTrigger.Interval);
            if (delayedTrigger.Interval < 0)
            {
                delayedTrigger.Interval = 0;
            }
            delayedTrigger.Loop = EditorUtils.Checkbox(this,"Loop", delayedTrigger.Loop);
            if (!delayedTrigger.Loop)
            {
                delayedTrigger.Times= EditorUtils.IntField(this,"Times",delayedTrigger.Times);
            }
            if (delayedTrigger.Times < 0)
            {
                delayedTrigger.Times = 0;
            }
            delayedTrigger.TriggerOnLoopEnd = EditorUtils.SelectTrigger(this, "On Loop End", delayedTrigger.TriggerOnLoopEnd);
        }
        StorePrefabConflict(delayedTrigger);
    }
}
