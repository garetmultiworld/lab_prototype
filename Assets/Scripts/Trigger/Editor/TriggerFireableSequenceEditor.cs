﻿using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(TriggerFireableSequence), true)]
public class TriggerFireableSequenceEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(TriggerFireableSequence trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "TriggerFireableSequence");
    }

    protected void StorePrefabConflict(TriggerFireableSequence trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        TriggerFireableSequence triggerFireableSequence = (TriggerFireableSequence)target;
        SetUpPrefabConflict(triggerFireableSequence);
        triggerFireableSequence.showTriggerFireableSequence = EditorGUILayout.Foldout(
            triggerFireableSequence.showTriggerFireableSequence,
            "Triggers"
        );
        if (triggerFireableSequence.showTriggerFireableSequence)
        {
            triggerFireableSequence.loop = EditorUtils.Checkbox(this,"Loop", triggerFireableSequence.loop);
            if (triggerFireableSequence.Triggers.Length == 0)
            {
                if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref triggerFireableSequence.Triggers, triggerFireableSequence.Triggers.Length + 1);
                    _hadChanges = true;
                }
            }
            for (int iTrigger = 0; iTrigger < triggerFireableSequence.Triggers.Length; iTrigger++)
            {
                DrawItem(triggerFireableSequence, iTrigger);
            }
        }
        StorePrefabConflict(triggerFireableSequence);
    }

    protected void DrawItem(TriggerFireableSequence triggerFireableSequence, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref triggerFireableSequence.Triggers, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref triggerFireableSequence.Triggers, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref triggerFireableSequence.Triggers, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref triggerFireableSequence.Triggers, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (triggerFireableSequence.Triggers.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref triggerFireableSequence.Triggers, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(triggerFireableSequence, iTrigger);
        }
    }

    protected void DrawItemDetails(TriggerFireableSequence triggerFireableSequence, int iTrigger)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(iTrigger.ToString());
        TriggerFireableSequenceItem item;
        if (triggerFireableSequence.Triggers[iTrigger] == null)
        {
            triggerFireableSequence.Triggers[iTrigger] = new TriggerFireableSequenceItem();
        }

        item = triggerFireableSequence.Triggers[iTrigger];

        item.Label = EditorUtils.TextField(this,"Name", item.Label);
        GUILayout.EndHorizontal();

        item.TriggerToFire = EditorUtils.TriggerField(this,item.TriggerToFire);
        
        EditorUtils.TriggerDescription(item.TriggerToFire);

        if (iTrigger < (triggerFireableSequence.Triggers.Length - 1))
        {
            EditorUtils.DrawUILine(Color.grey);
        }
    }
}
