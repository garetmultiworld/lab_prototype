﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ChanceInGroupTrigger), true)]
public class ChanceInGroupTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(ChanceInGroupTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ChanceInGroupTrigger");
    }

    protected void StorePrefabConflict(ChanceInGroupTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        ChanceInGroupTrigger chanceInGroupTrigger = (ChanceInGroupTrigger)target;
        SetUpPrefabConflict(chanceInGroupTrigger);
        chanceInGroupTrigger.showChanceInGroupTrigger = EditorGUILayout.Foldout(
            chanceInGroupTrigger.showChanceInGroupTrigger,
            "Items (" + chanceInGroupTrigger.Items.Length.ToString() + ")"
        );
        if (chanceInGroupTrigger.showChanceInGroupTrigger)
        {
            if (chanceInGroupTrigger.Items.Length != chanceInGroupTrigger.showChanceGroupTriggerItem.Length)
            {
                Array.Resize(ref chanceInGroupTrigger.showChanceGroupTriggerItem, chanceInGroupTrigger.Items.Length);
            }
            if (chanceInGroupTrigger.Items.Length == 0)
            {
                if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref chanceInGroupTrigger.Items, chanceInGroupTrigger.Items.Length + 1);
                    Array.Resize(ref chanceInGroupTrigger.showChanceGroupTriggerItem, chanceInGroupTrigger.showChanceGroupTriggerItem.Length + 1);
                    _hadChanges = true;
                }
            }
            if (chanceInGroupTrigger.Items.Length > 0)
            {
                if (GUILayout.Button("Equal Chances"))
                {
                    float newChance = 100f / (float)chanceInGroupTrigger.Items.Length;
                    for (int iItem = 0; iItem < chanceInGroupTrigger.Items.Length; iItem++)
                    {
                        chanceInGroupTrigger.Items[iItem].Chances = newChance;
                    }
                    _hadChanges = true;
                }
            }
            for (int iTrigger = 0; iTrigger < chanceInGroupTrigger.Items.Length; iTrigger++)
            {
                DrawItem(chanceInGroupTrigger, iTrigger);
            }
        }
        StorePrefabConflict(chanceInGroupTrigger);
    }

    protected void DrawItem(ChanceInGroupTrigger groupTrigger, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref groupTrigger.Items, iTrigger);
            EditorArrayUtils.RemoveAt(ref groupTrigger.showChanceGroupTriggerItem, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref groupTrigger.Items, iTrigger);
            EditorArrayUtils.InsertBefore(ref groupTrigger.showChanceGroupTriggerItem, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref groupTrigger.Items, iTrigger);
            EditorArrayUtils.InsertAfter(ref groupTrigger.showChanceGroupTriggerItem, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref groupTrigger.Items, iTrigger);
                EditorArrayUtils.MoveUp(ref groupTrigger.showChanceGroupTriggerItem, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (groupTrigger.Items.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref groupTrigger.Items, iTrigger);
                EditorArrayUtils.MoveDown(ref groupTrigger.showChanceGroupTriggerItem, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(groupTrigger, iTrigger);
        }
    }

    protected void DrawItemDetails(ChanceInGroupTrigger chanceInGroupTrigger, int iTrigger)
    {
        chanceInGroupTrigger.showChanceGroupTriggerItem[iTrigger] = EditorGUILayout.Foldout(
            chanceInGroupTrigger.showChanceGroupTriggerItem[iTrigger],
            iTrigger.ToString()
        );
        if (chanceInGroupTrigger.showChanceGroupTriggerItem[iTrigger])
        {
            if (chanceInGroupTrigger.Items[iTrigger] == null)
            {
                chanceInGroupTrigger.Items[iTrigger] = new ChanceGroupTriggerItem();
                _hadChanges = true;
            }

            chanceInGroupTrigger.Items[iTrigger].TriggerToFire = EditorUtils.SelectTrigger(this, "Trigger To Fire", chanceInGroupTrigger.Items[iTrigger].TriggerToFire);
            chanceInGroupTrigger.Items[iTrigger].Chances = EditorUtils.Slider(this, "Chances", chanceInGroupTrigger.Items[iTrigger].Chances, 0f, 100f);

            if (iTrigger < (chanceInGroupTrigger.Items.Length - 1))
            {
                EditorUtils.DrawUILine(Color.grey);
            }
        }
        
    }
}
