﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSequence : TriggerInterface
{

    public TriggerSequenceItem[] Triggers;
    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        StartCoroutine(SequenceFire());
    }

    private IEnumerator SequenceFire()
    {
        int itemIndex = 0;
        float time = 0;
        for (itemIndex = 0; itemIndex < Triggers.Length && IsPlaying; itemIndex++)
        {
            for (time = 0; time < Triggers[itemIndex].TimeBefore && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
            if (IsPlaying)
            {
                Triggers[itemIndex].Fire(this);
            }
            for (time = 0; time < Triggers[itemIndex].TimeAfter && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[Triggers.Length];
        for (int i = 0; i < Triggers.Length; i++)
        {
            callbacks[i] = new TriggerCallback(i.ToString(), Triggers[i].TriggerToFire, i);
        }
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        Triggers[index].TriggerToFire = trigger;
    }
#endif

}
