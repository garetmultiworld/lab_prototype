﻿using System.Collections;
using UnityEngine;

public class CooldownTrigger : TriggerInterface
{

    public TriggerInterface Target;
    public float Cooldown=1;

    protected bool IsPlaying=false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Target.IsDisabled = true;
        IsDisabled = true;
        IsPlaying = true;
        StartCoroutine(CountCooldown());
    }

    private IEnumerator CountCooldown()
    {
        float time;
        for (time = Cooldown; time > 0 && IsPlaying; time -= Time.deltaTime)
        {
            yield return null;
        }
        Target.IsDisabled = false;
        IsDisabled = false;
    }

}