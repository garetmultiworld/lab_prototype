﻿using System.Collections;
using UnityEngine;

public class DelayedTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showDelayedTrigger = true;
#endif

    public TriggerInterface TriggerToFire;
    public float Interval=1;
    public bool Loop;
    public int Times=1;
    public TriggerInterface TriggerOnLoopEnd;

    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        StartCoroutine(DelayedFire());
    }

    private IEnumerator DelayedFire()
    {
        float count;
        for (count = 0; (Loop || count < Times) && IsPlaying; count++)
        {
            float time;
            for (time = 0; time < Interval && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
            if (IsPlaying)
            {
                TriggerToFire.Fire(this);
            }
        }
        if (TriggerOnLoopEnd != null)
        {
            TriggerOnLoopEnd.Fire(this);
        }
        IsPlaying = false;
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToFire == null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += ", " + Interval.ToString() + "s";
            if (Loop)
            {
                desc += ", loop";
            }
            else
            {
                desc += ", " + Times.ToString() + " times";
            }
            desc += " (" + TriggerToFire.GetLabel() + ")";
        }
        return desc;
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("Trigger", TriggerToFire,0);
        callbacks[1] = new TriggerCallback("On Loop End", TriggerOnLoopEnd,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            TriggerToFire = trigger;
        }
        else
        {
            TriggerOnLoopEnd = trigger;
        }
    }
#endif

}
