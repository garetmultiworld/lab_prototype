﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface TriggerInvoker
{

#if UNITY_EDITOR
    TriggerCallback[] GetCallbacks();

    void SetCallback(int index,TriggerInterface trigger);

    Vector2 GetNodeEditorPos();
    
    void SetNodeEditorPos(Vector2 pos);
#endif

}
