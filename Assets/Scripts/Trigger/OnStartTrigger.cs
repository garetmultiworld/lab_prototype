﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartTrigger : MonoBehaviour, TriggerInvoker
{

#if UNITY_EDITOR
    [HideInInspector]
    public Vector2 nodeEditorPos;
#endif

    public static string[] TriggerCategories = { "Trigger Invoker", "Game Flow" };

    public TriggerInterface Trigger;

    void Start()
    {
        if (Trigger != null)
        {
            Trigger.Fire();
        }
    }

#if UNITY_EDITOR
    public TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("Trigger", Trigger, 0);
        return callbacks;
    }

    public void SetCallback(int index, TriggerInterface trigger)
    {
        Trigger = trigger;
    }

    public virtual Vector2 GetNodeEditorPos()
    {
        return nodeEditorPos;
    }

    public virtual void SetNodeEditorPos(Vector2 pos)
    {
        nodeEditorPos = pos;
    }
#endif

}
