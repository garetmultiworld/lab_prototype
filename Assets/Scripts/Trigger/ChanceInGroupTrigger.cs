﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceInGroupTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showChanceInGroupTrigger = true;
#endif
#if UNITY_EDITOR
    public bool[] showChanceGroupTriggerItem=new bool[0];
#endif

    public ChanceGroupTriggerItem[] Items=new ChanceGroupTriggerItem[0];

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        float chance = Random.Range(0f, 100f);
        float currentChance = 0f;
        for (int i = 0; i < Items.Length; i++)
        {
            if (chance <= (Items[i].Chances + currentChance))
            {
                Items[i].Fire(this);
                return;
            }
            currentChance += Items[i].Chances;
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[Items.Length];
        for(int i=0;i<Items.Length;i++)
        {
            callbacks[i] = new TriggerCallback(Items[i].Chances.ToString(), Items[i].TriggerToFire,i);
        }
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        Items[index].TriggerToFire = trigger;
    }
#endif

}
