﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FunctionTrigger : TriggerInterface
{

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public UnityEvent TheFunctions;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheFunctions.Invoke();
    }

}
