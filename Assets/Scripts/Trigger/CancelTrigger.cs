﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showCancelTrigger = true;
#endif

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public TriggerInterface TriggerToCancel;

    public override void Cancel()
    {}

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TriggerToCancel.Cancel();
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToCancel==null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " ("+TriggerToCancel.GetLabel()+")";
        }
        return desc;
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("Trigger to cancel", TriggerToCancel, 0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerToCancel = trigger;
    }
#endif

}
