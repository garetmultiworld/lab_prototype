﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Multiworld/Inventory")]
public class Inventory : ScriptableObject
{

    public string Name="";
    public List<InventoryItem> items=new List<InventoryItem>();
    public InventoryDictionary dictionary;
    protected List<InventoryItemUpdater> Updaters=new List<InventoryItemUpdater>();
    public bool AllowZeroQuantity = true;

    public Inventory(Inventory reference=null)
    {
        if (reference != null)
        {
            SetData(reference);
        }
    }

    public void SetData(Inventory reference)
    {
        Name = reference.Name;
        items = new List<InventoryItem>();
        foreach (InventoryItem item in reference.items)
        {
            items.Add(new InventoryItem(item));
        }
        dictionary = reference.dictionary;
        InitUpdaters();
    }

    public Inventory(InventoryData data)
    {
        SetData(data);
    }

    public void SetData(InventoryData data)
    {
        Name = data.Name;
        InventoryItem newItem;
        items = new List<InventoryItem>();
        foreach (InventoryItemData item in data.items)
        {
            newItem = new InventoryItem(item);
            items.Add(newItem);
        }
        InitUpdaters();
    }

    public void RegisterUpdater(InventoryItemUpdater updater)
    {
        Updaters.Add(updater);
        InitUpdaters();
    }

    public void InitUpdaters()
    {
        if (Updaters == null)
        {
            Updaters = new List<InventoryItemUpdater>();
        }
        foreach (InventoryItem item in items)
        {
            foreach (InventoryItemUpdater updater in Updaters)
            {
                if (updater.ItemName.Equals(item.Name))
                {
                    updater.slider.value = item.Amount;
                    break;
                }
            }
        }
    }

    public void Clean()
    {
        items.Clear();
    }

    public bool HasItems(InventoryItem[] compareItems)
    {
        bool itHas=true;
        bool hasIndividual;
        foreach (InventoryItem compareItem in compareItems)
        {
            hasIndividual = false;
            foreach (InventoryItem item in items)
            {
                if (item.Name.Equals(compareItem.Name)&& item.Amount>=compareItem.Amount)
                {
                    hasIndividual = true;
                    break;
                }
            }
            if (!hasIndividual)
            {
                itHas = false;
                break;
            }
        }
        return itHas;
    }

    public int GetItemAmount(string Name)
    {
        int amount = 0;
        foreach (InventoryItem item in items)
        {
            if (item.Name.Equals(Name))
            {
                amount=item.Amount;
                break;
            }
        }
        return amount;
    }

    public GameObject GetItemUIPrefab(string Name)
    {
        foreach(InventoryDictionaryItem item in dictionary.items)
        {
            if (item.Name.Equals(Name))
            {
                return item.UIPrefab;
            }
        }
        return null;
    }

    public void AddItem(string Name,int Amount)
    {
        int maxStackAmount=1;
        foreach (InventoryDictionaryItem item in dictionary.items)
        {
            if (item.Name.Equals(Name))
            {
                maxStackAmount = item.MaxStackAmount;
                break;
            }
        }
        foreach(InventoryItem item in items)
        {
            if (item.Name.Equals(Name))
            {
                if(item.Amount + Amount <= maxStackAmount)
                {
                    item.Amount += Amount;
                    if (item.Amount < 0)
                    {
                        item.Amount = 0;
                        if (!AllowZeroQuantity)
                        {
                            items.Remove(item);
                        }
                    }
                    if (Updaters != null)
                    {
                        foreach (InventoryItemUpdater updater in Updaters)
                        {
                            if (updater.ItemName.Equals(Name))
                            {
                                updater.slider.value = item.Amount;
                                break;
                            }
                        }
                    }
                    return;
                }
                else
                {
                    break;
                }
            }
        }
        if (Amount < 0)
        {
            return;
        }
        InventoryItem newItem = new InventoryItem();
        newItem.Name= Name;
        newItem.Amount = Amount;
        items.Add(newItem);
    }

    public bool HasItemsInTierRange(List<int> Tiers)
    {
        foreach(InventoryItem item in items)
        {
            if (item.Amount>0&&Tiers.Contains(item.Tier))
            {
                return true;
            }
        }
        return false;
    }

    public int removeRandom(int TierToRemove, int AmountToRemove)
    {
        int removed = 0;
        List<int> removableItems = new List<int>();
        for (int iitem = 0; iitem < items.Count; iitem++)
        {
            if (items[iitem].Amount > 0 && items[iitem].Tier == TierToRemove)
            {
                removableItems.Add(iitem);
            }
        }
        if (removableItems.Count > 0)
        {
            int indexToRemove = Random.Range(0, removableItems.Count);
            items[indexToRemove].Amount -= AmountToRemove;
            if (items[indexToRemove].Amount > 0)
            {
                removed = AmountToRemove;
            }
            else
            {
                if (items[indexToRemove].Amount == 0)
                {
                    removed = AmountToRemove;
                }
                else
                {
                    removed = AmountToRemove - items[indexToRemove].Amount;
                }
            }
            if (items[indexToRemove].Amount < 0)
            {
                items[indexToRemove].Amount = 0;
            }
        }
        return removed;
    }

}
