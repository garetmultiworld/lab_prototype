using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(AddInventoryTrigger), true)]
public class AddInventoryTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(AddInventoryTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AddInventoryTrigger");
    }

    protected void StorePrefabConflict(AddInventoryTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        AddInventoryTrigger addInventoriesTrigger = (AddInventoryTrigger)target;
        SetUpPrefabConflict(addInventoriesTrigger);
        addInventoriesTrigger.showAddInventoryTrigger = EditorGUILayout.Foldout(
            addInventoriesTrigger.showAddInventoryTrigger,
            "Item"
        );
        if (addInventoriesTrigger.showAddInventoryTrigger)
        {
            addInventoriesTrigger.inventory = EditorUtils.InventoryField(this, "Inventory", addInventoriesTrigger.inventory);
            addInventoriesTrigger.Item = EditorUtils.TextField(this, "Item", addInventoriesTrigger.Item);
            addInventoriesTrigger.Amount = EditorUtils.IntField(this, "damageAmount", addInventoriesTrigger.Amount);
        }
        StorePrefabConflict(addInventoriesTrigger);
    }
    
}
