using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(AddInventoriesTrigger), true)]
public class AddInventoriesTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(AddInventoriesTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AddInventoriesTrigger");
    }

    protected void StorePrefabConflict(AddInventoriesTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        AddInventoriesTrigger addInventoriesTrigger = (AddInventoriesTrigger)target;
        SetUpPrefabConflict(addInventoriesTrigger);
        addInventoriesTrigger.showAddInventoriesTrigger = EditorGUILayout.Foldout(
            addInventoriesTrigger.showAddInventoriesTrigger,
            "Items (" + addInventoriesTrigger.items.Length.ToString() + ")"
        );
        if (addInventoriesTrigger.showAddInventoriesTrigger)
        {
            addInventoriesTrigger.inventory = EditorUtils.InventoryField(this, "Inventory", addInventoriesTrigger.inventory);
            if (addInventoriesTrigger.items.Length == 0)
            {
                if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref addInventoriesTrigger.items, addInventoriesTrigger.items.Length + 1);
                    _hadChanges = true;
                }
            }
            for (int iTrigger = 0; iTrigger < addInventoriesTrigger.items.Length; iTrigger++)
            {
                DrawItem(addInventoriesTrigger, iTrigger);
            }
        }
        StorePrefabConflict(addInventoriesTrigger);
    }

    protected void DrawItem(AddInventoriesTrigger groupTrigger, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref groupTrigger.items, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref groupTrigger.items, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref groupTrigger.items, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref groupTrigger.items, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (groupTrigger.items.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref groupTrigger.items, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(groupTrigger, iTrigger);
        }
    }

    protected void DrawItemDetails(AddInventoriesTrigger addInventoriesTrigger, int iInventory)
    {
        GUILayout.BeginHorizontal();

        if (addInventoriesTrigger.items[iInventory]==null)
        {
            addInventoriesTrigger.items[iInventory] = new InventoryItem();
        }

        GUILayout.Label(iInventory.ToString());
        
        GUILayout.EndHorizontal();

        addInventoriesTrigger.items[iInventory].Name = EditorUtils.TextField(this, "Name", addInventoriesTrigger.items[iInventory].Name);
        addInventoriesTrigger.items[iInventory].Amount = EditorUtils.IntField(this, "damageAmount", addInventoriesTrigger.items[iInventory].Amount);
        addInventoriesTrigger.items[iInventory].Tier = EditorUtils.IntField(this, "Tier", addInventoriesTrigger.items[iInventory].Tier);

        if (iInventory < (addInventoriesTrigger.items.Length - 1))
        {
            EditorUtils.DrawUILine(Color.grey);
        }
    }

}
