﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{

    #region Static Instance
    private static InventoryManager instance;
    public static InventoryManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InventoryManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned InventoryManager", typeof(InventoryManager)).GetComponent<InventoryManager>();
                    DontDestroyOnLoad(instance);
                }
            }
            instance.Initialize();
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    [HideInInspector]
    public Dictionary<string, Inventory> Inventories = new Dictionary<string, Inventory>();

    InventoryManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Initialize()
    {
        //
    }

    public void RegisterUpdater(InventoryItemUpdater updater)
    {
        foreach (Inventory inventory in Inventories.Values)
        {
            inventory.RegisterUpdater(updater);
        }
    }

    public Inventory getInventory(Inventory reference)
    {
        Inventory inventory;
        if (!Inventories.ContainsKey(reference.Name))
        {
            inventory = ScriptableObject.CreateInstance("Inventory") as Inventory;
            inventory.SetData(reference);
            Inventories.Add(reference.Name, inventory);
        }
        else
        {
            inventory = Inventories[reference.Name];
        }
        return inventory;
    }

    public void ClearData()
    {
        Inventories.Clear();
    }

    public void LoadData(InventoryData[] inventories)
    {
        Inventories.Clear();
        Inventory newInventory;
        if (inventories == null)
        {
            return;
        }
        foreach (InventoryData inventory in inventories)
        {
            newInventory = getInventory(inventory.GetInventory());
            newInventory.SetData(inventory);
        }
    }

    public void SaveData(InventoryData[] inventories)
    {
        int index = 0;
        int itemIndex;
        InventoryData newInventory;
        InventoryItemData newInventoryItem;
        foreach (Inventory inventory in Inventories.Values)
        {
            newInventory = new InventoryData();
            newInventory.Name = inventory.Name;
            newInventory.items = new InventoryItemData[inventory.items.Count];
            itemIndex = 0;
            foreach (InventoryItem item in inventory.items)
            {
                newInventoryItem = new InventoryItemData();
                newInventoryItem.Name = item.Name;
                newInventoryItem.Amount = item.Amount;
                newInventoryItem.Tier = item.Tier;
                newInventory.items[itemIndex] = newInventoryItem;
                itemIndex++;
            }
            inventories[index] = newInventory;
            index++;
        }
    }
}
