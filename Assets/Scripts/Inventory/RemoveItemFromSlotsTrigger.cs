﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveItemFromSlotsTrigger : TriggerInterface
{

    public string ItemName;
    public GameObject SlotsParent;

    private Transform parentTransform;

    private void Start()
    {
        if (SlotsParent != null)
        {
            parentTransform = SlotsParent.transform;
        }
        else
        {
            parentTransform = transform;
        }
    }

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (Transform child in parentTransform)
        {
            DraggableInventorySlot slot=child.GetComponent<DraggableInventorySlot>();
            if (slot != null&&slot.Occupied&& ItemName.Equals(slot.Name))
            {
                slot.ClearSlot();
                return;
            }
        }
    }

}
