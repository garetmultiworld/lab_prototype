﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickPickable2D : MonoBehaviour
{

    public string Name = "";
    public int Amount;

    private Player player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player tmp = collision.GetComponent<Player>();
        if (tmp != null)
        {
            player = tmp;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Player tmp = collision.GetComponent<Player>();
        if (tmp != null)
        {
            player = null;
        }
    }

    void OnMouseDown()
    {
        if (player != null)
        {
            InventoryManager.Instance.getInventory(player.inventories[0]).AddItem(Name, Amount);
            Destroy(gameObject);
        }
    }
}
