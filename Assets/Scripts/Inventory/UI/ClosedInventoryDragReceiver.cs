﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosedInventoryDragReceiver : MonoBehaviour
{

    public static ClosedInventoryDragReceiver Instance;

    public TriggerInterface OpenTrigger;

    private RectTransform rect;

    private void Start()
    {
        Instance = this;
        rect = this.GetComponent<RectTransform>();
    }

    public static bool CheckIfWithinBounds(Vector3 mousePosition)
    {
        if (!Instance.gameObject.activeSelf)
        {
            return false;
        }
        /* Vector2 normalizedMousePosition = new Vector2(mousePosition.x / Screen.width, mousePosition.y / Screen.height);
         bool withinBounds = normalizedMousePosition.x > Instance.rect.anchorMin.x &&
             normalizedMousePosition.x < Instance.rect.anchorMax.x &&
             normalizedMousePosition.y > Instance.rect.anchorMin.y &&
             normalizedMousePosition.y < Instance.rect.anchorMax.y;
         */
        /*
         Vector2 localPoint;
         RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransfiorm, Input.mousePosition, Camera.main, out localPoint);
         return _rectTransfiorm.rect.Contains(localPoint);
         */
        Vector2 localMousePosition = Instance.rect.InverseTransformPoint(mousePosition);
        bool withinBounds = Instance.rect.rect.Contains(localMousePosition);
        if (withinBounds && Instance.OpenTrigger != null)
        {
            Instance.OpenTrigger.Fire();
        }
        return withinBounds;
    }

}
