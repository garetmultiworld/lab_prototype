﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory Dictionary", menuName = "Multiworld/Inventory Dictionary")]
public class InventoryDictionary : ScriptableObject
{

    public string Name = "";
    public List<InventoryDictionaryItem> items = new List<InventoryDictionaryItem>();

}
