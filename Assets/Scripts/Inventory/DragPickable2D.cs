using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragPickable2D : MonoBehaviour
{

    public string Name = "";
    public int Amount;
    public GameObject ArtToInventory;
    public bool CanBePicked=true;
    public bool CanReceive=false;
    public TriggerInterface OnPlayerInRange;
    public TriggerInterface OnPlayerOutOfRange;
    public TriggerInterface OnBeginDrag;
    public TriggerInterface OnEndDrag;
    public TriggerInterface OnDropInInventorySlot;
    public TriggerInterface OnReceiveFromInventory;

    private Player player;

    private Vector3 OriginalPosition;
    private Transform originalParent;
    private bool dragging=false;

    private void Awake()
    {
        originalParent = this.transform.parent;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player tmp = collision.GetComponent<Player>();
        if (tmp != null)
        {
            player = tmp;
            if (OnPlayerInRange != null)
            {
                OnPlayerInRange.Fire();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Player tmp = collision.GetComponent<Player>();
        if (tmp != null)
        {
            player = null;
            if (OnPlayerOutOfRange != null)
            {
                OnPlayerOutOfRange.Fire();
            }
        }
    }

    public void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                return;
        }
        if (player != null && !dragging && ArtToInventory.activeSelf && CanBePicked)
        {
            OriginalPosition = this.transform.position;
            this.transform.SetParent(null);
            dragging = true;
            if (OnBeginDrag != null)
            {
                OnBeginDrag.Fire();
            }
        }
    }

    public void OnMouseUp()
    {
        if (dragging)
        {
            dragging = false;
            transform.SetParent(originalParent);
            this.transform.position = OriginalPosition;
            if (OnEndDrag != null)
            {
                OnEndDrag.Fire();
            }
            if (DraggableInventorySlot.CheckIfWithinBounds(Input.mousePosition, this))
            {
                if (OnDropInInventorySlot != null)
                {
                    OnDropInInventorySlot.Fire();
                }
                ArtToInventory.SetActive(false);
            }
        }
    }

    public void Receive()
    {
        Debug.Log("Can receive");
        Debug.Log(CanReceive);
        Debug.Log("ArtToInventory.activeSelf");
        Debug.Log(ArtToInventory.activeSelf);
        if (CanReceive&&!ArtToInventory.activeSelf)
        {
            Debug.Log("Receiving...");
            ArtToInventory.SetActive(true);
            if (OnReceiveFromInventory != null)
            {
                Debug.Log("Triggering...");
                OnReceiveFromInventory.Fire();
            }
        }
    }

    void Update()
    {
        if (dragging)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePosition);
            ClosedInventoryDragReceiver.CheckIfWithinBounds(Input.mousePosition);
        }
    }

    public void SetCanBePicked(bool canBePicked)
    {
        CanBePicked = canBePicked;
    }

    public void SetCanReceive(bool canReceive)
    {
        CanReceive = canReceive;
    }

}
