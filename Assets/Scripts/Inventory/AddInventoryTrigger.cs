﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddInventoryTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showAddInventoryTrigger = true;
#endif

    public static string[] TriggerCategories = { "Trigger", "Inventory" };

    public Inventory inventory;
    public string Item;
    public int Amount;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        realInventory.AddItem(Item, Amount);
    }

}
