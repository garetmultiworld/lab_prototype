﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignItemFromEvent : TriggerInterface
{

    public static string[] TriggerCategories = { "Trigger", "Inventory" };

    public bool StacksAllowed = false;

    public DraggableInventorySlot[] slots;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }

        DraggableInventorySlot slot;

        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]!=null&&(!slots[i].Occupied||(StacksAllowed&&slots[i].Name.Equals(StringParameter))))
            {
                slot = slots[i];
                if (StacksAllowed)
                {
                    slot.AddItem(StringParameter, IntParameter);
                }
                else
                {
                    slot.AddItem(StringParameter, 1);
                }
                break;
            }
        }
    }

}
