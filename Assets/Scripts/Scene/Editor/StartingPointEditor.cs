﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(StartingPoint))]
public class StartingPointEditor : EditorBase
{

    protected void SetUpPrefabConflict(StartingPoint startingPoint)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(startingPoint, "StartingPoint");
    }

    protected void StorePrefabConflict(StartingPoint startingPoint)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(startingPoint);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(startingPoint.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        StartingPoint startingPoint = (StartingPoint)target;
        SetUpPrefabConflict(startingPoint);
        startingPoint.Label = EditorUtils.TextField(this,"Label", startingPoint.Label);
        startingPoint.TriggerOnArrive = EditorUtils.SelectTrigger(this,"On Arrive", startingPoint.TriggerOnArrive);
        StorePrefabConflict(startingPoint);
    }

}
