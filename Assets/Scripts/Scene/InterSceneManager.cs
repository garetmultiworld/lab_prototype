﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterSceneManager : MonoBehaviour
{

    #region Static Instance
    private static InterSceneManager instance;
    public static InterSceneManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InterSceneManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned InterSceneManager", typeof(InterSceneManager)).GetComponent<InterSceneManager>();
                    DontDestroyOnLoad(instance);
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    [HideInInspector]
    public string NextStartingPoint;
    [HideInInspector]
    public string NextScene;
    [HideInInspector]
    public string[] SceneLoadExceptions;

    private GameObject LevelStart;

    //first
    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //second
    /*void OnEnable()
    {
        Debug.Log("OnEnable called");
    }*/

    //fourth
    /*void Start()
    {
        Debug.Log("Start");
    }*/

    //third
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals(NextScene))
        {
            bool isException = false;
            Debug.Log(SceneLoadExceptions);
            foreach (string sceneException in SceneLoadExceptions)
            {
                Debug.Log("sceneException: "+ sceneException);
                if (sceneException.Equals(NextScene))
                {
                    isException = true;
                    break;
                }
            }
            if (!isException)
            {
                PersistanceManager.Instance.dataToSave.NextStartingPoint = NextStartingPoint;
                PersistanceManager.Instance.dataToSave.SceneToLoad = NextScene;
                PersistanceManager.Instance.SaveData();
            }
            var startingPoints = FindObjectsOfType<StartingPoint>();
            foreach (StartingPoint startingPoint in startingPoints)
            {
                if (startingPoint.Label.Equals(NextStartingPoint))
                {
                    PlayerManager.Instance.GetPlayer(0).transform.position = startingPoint.transform.position;
                    if (startingPoint.TriggerOnArrive != null)
                    {
                        startingPoint.TriggerOnArrive.Fire();
                    }
                    break;
                }
            }
        }
    }

}
