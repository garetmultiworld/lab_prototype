using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mw.player;

public class PlayerActiveState : CharacterState
{
    [Header("Attributes")]

    //Esto de aca maneja el movimiento y la camara
    public float speed = 6f;
    public float idle2 = 20f;
    //Esto de aca maneja el Jump
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float attackDelay = 1.5f;
    public float comboGapRangeMin = 0.3f;
    public float comboGapRangeMax = 0.8f;
    public float dashSpeed;
    public float dashTime;

    [Header("Conections")]

    public Animator animator;
    public CharacterController controller;
    public Transform cam;
    public Transform groundCheck;
    public LayerMask groundMask;
    public GameObject DamageVolumeGameObject;

    [Header("Unity Fields")]

    public float groundDistance = 0.4f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    Vector3 velocity;
    Vector3 moveDir;
    bool isGrounded = true;
    float contador = 0f;
    bool isAttacking = false;
    float _attack = 0f; 
    bool canConnectAttack = false;
    
    int comboCount = 0;
    float attackDelayHolder;
    bool isRunning = false;
    [HideInInspector]
    public bool ComboActive = false;

    [HideInInspector]
    public bool canMove = true;

    private void Start()
    {
        attackDelayHolder = attackDelay;
    }

    //Funci�n Movement, aqui esta incluido el comportamiento de la camara.
    public void Movement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (vertical != 0)
        {
            animator.SetFloat("Speed", Mathf.Abs(vertical * speed));
        }
        else
        {
            animator.SetFloat("Speed", Mathf.Abs(horizontal * speed));
        }

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            controller.Move(moveDir.normalized * speed * Time.deltaTime);

            contador = 0;
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }
    }

    //Funci�n Jump
    public void Jump()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        animator.SetBool("Jump", !isGrounded);
        

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }


        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            animator.Play("Base Layer.Jump");
            contador = 0;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    // Esta funcion maneja el Idle 2
    public void Rest()
    {
        contador += Time.deltaTime;
        if (contador > idle2)
        {
            animator.SetBool("Rest", true);
            contador = 0;
        }
        else
        {
            animator.SetBool("Rest", false);
        }


    }

    //Esto maneja el Melee Attack
    public void MeleeAttack()
    {        
        //Esto es el core del melee attack combo
        if (Input.GetButtonDown("Attack") && isGrounded)
        {
            DamageVolumeGameObject.SetActive(true);

            if (!canConnectAttack && !isAttacking && !ComboActive)
            {
                if (isRunning)
                {
                    animator.Play("UpperBody.Attack");
                    canMove = true;
                }
                else
                {
                    animator.Play("Base Layer.Attack0");
                    canMove = false;
                }
                
                
            }
            else if (canConnectAttack)
            {
                animator.SetTrigger("AttackCombo"); 
            }
            isAttacking = true;
            attackDelayHolder = attackDelay;
        }
        //Esto maneja la ventana de tiempo del combo gap Range y el bloqueo de movimiento
        if (isAttacking)
        {
            _attack += Time.deltaTime;
            attackDelayHolder -= Time.deltaTime;
            animator.SetBool("isAttacking", true);

            if (_attack >= comboGapRangeMin && _attack < comboGapRangeMax)
            {
                canConnectAttack = true;                
            }
            else if(_attack >= comboGapRangeMax)
            {
                canConnectAttack = false;                
                
                _attack = 0f;
                
            }
            if(attackDelayHolder <= 0)            
            {
                attackDelayHolder = attackDelay;
                canConnectAttack = false;                
                animator.SetBool("isAttacking", false);
                isAttacking = false;                
            }
        }
        else
        {            
            _attack = 0f;
        }
         
        
    }

    public void AirMeleeAttack()
    {
        if (animator.GetBool("Jump") && Input.GetButton("Attack"))
        {
            //aqui debo hacer que el rigid body se vuelva kinematic
            //luego debo hacer que ataque en combo
            //y por ultimo que caiga duro al suelo
        }
    }

    public void Dash()
    {
        if (Input.GetButtonDown("Dash"))
        {
            StartCoroutine(DashCo());
            animator.SetTrigger("Dash");
        }
    }

    IEnumerator DashCo()
    {
        float startTime = Time.time;

        while(Time.time < startTime + dashTime)
        {
            controller.Move(moveDir * dashSpeed * Time.deltaTime);

            yield return null;
        }
    }
    public void Attack2Corroutine()
    {
        StartCoroutine(Attack2Co());
    }
    
    public IEnumerator Attack2Co()
    {
        float startTime = Time.time;

        while (Time.time < startTime + dashTime)
        {
            controller.Move(moveDir * 3f * Time.deltaTime);

            yield return null;
        }
    }
    public override void OnEnterState()
    {
        
    }

    public override void OnExitState()
    {
        
    }

    public override void OnFixedUpdate()
    {
        
    }

    public override void OnLateUpdate()
    {
        
    }

    public override void OnPauseState()
    {
        
    }

    public override void OnResumeState()
    {
        
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
        
    }

    public override void OnUpdate()
    {
        if (canMove)
        {
            Movement();
            Jump();
        } 
        Rest();
        MeleeAttack();
        Dash();
    }

}
