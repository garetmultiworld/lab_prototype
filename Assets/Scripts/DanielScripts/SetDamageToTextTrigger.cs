using UnityEngine;
using UnityEngine.UI;

public class SetDamageToTextTrigger : TriggerInterface
{    
    public Text text;   
    public float damage;
    string damageText;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        
        damageText = damage.ToString(); 
        text.text = damageText;
    }
}
