using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageArea : MonoBehaviour
{
    public TriggerInterface OnFinishFire;
    public TriggerInterface OnHitTrigger;
    public float Amount = 1;
    public LayerMask FilterLayer;
    public float ReloadTime = 0.3f;
    public float duration = 5f;
    
    private  List <Health> enemiesHealth = new List<Health>();
    private float reloadTimer = 0f;
    private float durationTimer = 0f;
    public bool canAttack = false;

   
    public GameObject parent;

    public void OnTriggerEnter(Collider c)
    {
        if((((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            Health h = c.GetComponent<Health>();
            if (h == null)
            {
                return;
            }
            if (enemiesHealth.IndexOf(h) != -1)
            {
                return;
            }

            enemiesHealth.Add(h);
        }
               
    }

    public void OnTriggerExit(Collider c)
    {
        if ((((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            enemiesHealth.Remove(c.GetComponent<Health>());
        }
    }
    private void OnEnable()
    {
        enemiesHealth.Clear();

    }

    private void OnDisable()
    {
        durationTimer = 0f;
        reloadTimer = 0f;
    }

    private void UpdateTargets()
    {
        List<Health> toRemove = new List<Health>();
        foreach (Health h in enemiesHealth)
        {
            if (
                h != null &&
                (
                    h.gameObject == null||
                    h.gameObject != null && !h.gameObject.activeSelf
                )
            )
            {
                toRemove.Add(h);
            }
        }
        foreach(Health h in toRemove)
        {
            enemiesHealth.Remove(h);
        }
    }

    private void Update()
    {
        reloadTimer += Time.deltaTime;
        durationTimer += Time.deltaTime;

        if(durationTimer >= duration)
        {
            durationTimer = 0f;
            if(OnFinishFire!=null)
            {
                OnFinishFire.Fire();
            }
            this.gameObject.SetActive(false);
            return;
        }

        if (reloadTimer >= ReloadTime)
        {
            reloadTimer = 0;
            UpdateTargets();
            foreach (Health h in enemiesHealth)
            {
                h.ReceiveDirectDamage(Amount, parent);
            }            
            if (OnHitTrigger != null)
            {
                OnHitTrigger.Fire();
            }
        }

        
    }

}
