using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    
    [Header("Attributes")]

    //Esto de aca maneja el movimiento y la camara
    public float speed = 6f;     
    public float idle2 = 20f;
    //Esto de aca maneja el Jump
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float attackDelay = 1.5f;

    [Header("Conections")]

    public Animator animator;
    public CharacterController controller;
    public Transform cam;
    public Transform groundCheck;
    public LayerMask groundMask;

    [Header("Unity Fields")]

    public float groundDistance = 0.4f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    Vector3 velocity;
    bool isGrounded = true;
    float contador = 0f;
    bool isAttacking = false;
    float _attack = 0f;
    

    void Update()
    {
        Movement();
        Jump();
        Rest();
        MeleeAttack();

    }

    //Funci�n Movement, aqui esta incluido el comportamiento de la camara.
    public void Movement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        
        if (vertical != 0)
        {
            animator.SetFloat("Speed", Mathf.Abs(vertical * speed));
        }
        else
        {
            animator.SetFloat("Speed", Mathf.Abs(horizontal * speed));
        }  

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            controller.Move(moveDir.normalized * speed * Time.deltaTime);

            contador = 0;
        }
    }

    //Funci�n Jump
    public void Jump()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        animator.SetBool("Jump", !isGrounded);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;            
        }
        

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            contador = 0;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    // Esta funcion maneja el Idle 2
    public void Rest()
    {
        contador += Time.deltaTime;
        if(contador > idle2)
        {
            animator.SetBool("Rest", true);
            contador = 0;
        }
        else
        {
            animator.SetBool("Rest", false);
        }
    
        
    }

    //Esto maneja el Melee Attack
    public void MeleeAttack()
    {        
        if (Input.GetButtonDown("Attack") && isGrounded && !isAttacking)
        {
            _attack = attackDelay;
            animator.SetTrigger("Attack");
            contador = 0;
        }

        if (isAttacking)
        {
            _attack -= Time.deltaTime;
        }

        if (_attack <= 0)
        {
            isAttacking = false;
        }
        else
        {
            isAttacking = true;
        }
    }
}
