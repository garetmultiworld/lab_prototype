using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    //Variables generales de comportamiento enemigo
    public float lookRadius = 5f;
    public Animator anim;
    Transform target;
    Transform player;
    Health health;
    NavMeshAgent agent;
    public float countDown = 2f;

    [Header("Asignar si es Melee")]
    public GameObject damageVolume;

    [HideInInspector]
    public bool canMove = false;
    public bool isAlive = true;
    float distance;
    float deathCountdown = 0f;

    //Variables para el patr�n de ataque tipo Torreta
    bool canAttack = false;
    Turret turret;
    float a;
    bool canDie = true;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerTag.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
        target = player;
        a = countDown;
        if (!canMove)
        {
            turret = GetComponent<Turret>();            
        }
    }

    // Update is called once per frame
    void Update()
    {              
        //Busca un nuevo Objetivo
        if(health.damageSource != null)
        {
            target = health.damageSource.transform;
        }
        else
        {
            target = player;

        }

        distance = Vector3.Distance(target.position, transform.position);
        if (isAlive)
        {
            if (canMove)
            {
                Melee();
            }
            else
            {
                Static();
            }
        }
        else
        {            
            deathCountdown += Time.deltaTime;
            if(deathCountdown >= 2f)
            {
                Destroy(gameObject);
            }
            else
            {
                Death();
            }
            
        }
    }
    void Death()
    {        
        if (canDie)
        {
            canAttack = false;
            canMove = false;
            anim.SetBool("isAttacking", false);
            anim.SetTrigger("Death");
            canDie = false;
        }        
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);        
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    void Melee()
    {
        if (canAttack)
        {
            damageVolume.SetActive(true);
        }
        else
        {
            while (a <= countDown && !canAttack)
            {
                a += Time.deltaTime;
            }
            damageVolume.SetActive(false);
            anim.SetBool("isAttacking", false);
        }

        //Comportamiento si es melee
        if (distance <= lookRadius)
        {
            agent.isStopped = false;
            agent.SetDestination(target.position);

            FaceTarget();            
            
            if (distance <= 3f)
            {

                if (a >= countDown)
                {
                    canAttack = true;
                    agent.isStopped = true;
                }
                if(canAttack && a > 0)
                {
                    anim.SetBool("isAttacking", true);
                    anim.SetBool("isWalking", false);
                    a -= Time.deltaTime;                    
                }
                else
                {                    
                    canAttack = false;
                    agent.isStopped = false;
                    anim.SetBool("isAttacking", false);
                }                
            }
            else
            {                
                if(canAttack && a > 0) 
                {
                    a -= Time.deltaTime;                    
                    agent.isStopped = true;
                    anim.SetBool("isAttacking", true);
                    anim.SetBool("isWalking", false);
                }
                else
                {
                    anim.SetBool("isAttacking", false);
                    anim.SetBool("isWalking", true);
                    canAttack = false;
                    agent.isStopped = false;
                }
            }
        }
        else
        {
            agent.isStopped = true;
            anim.SetBool("isWalking", false);
            anim.SetBool("isAttacking", false);
            canAttack = false;
            a = countDown;
        }

    }

    void Static()
    {

        //Comportamiento si es Static      

        if (distance <= lookRadius)
        {
            
            FaceTarget();

            if (a >= countDown)
            {
                canAttack = true;
            }
            

            if (canAttack && a > 0)
            {
                if (turret.fireCountdown <= 0f)
                {
                    turret.Shoot();
                    turret.fireCountdown = 1f / turret.fireRate;
                }

                turret.fireCountdown -= Time.deltaTime;
                anim.SetBool("isAttacking", true);
                a -= Time.deltaTime;
            }
            else
            {
                a += Time.deltaTime;
                canAttack = false;
                anim.SetBool("isAttacking", false);
            }

        }
        else
        {
            anim.SetBool("isAttacking", false);
            canAttack = false;
            a = 0f;
        }
    }
}
