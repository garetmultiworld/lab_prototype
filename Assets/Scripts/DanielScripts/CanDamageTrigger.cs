using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanDamageTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public DamageOnTouch weapon;
#endif

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public override void Cancel()
    { }

    public override void Fire()
    {

        if (!CanTrigger())
        {
            return;
        }
        
    }

    /*public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToCancel==null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " ("+TriggerToCancel.GetLabel()+")";
        }
        return desc;
    }*/

#if UNITY_EDITOR
    /*public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("Trigger to cancel", TriggerToCancel, 0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerToCancel = trigger;
    }*/
#endif

}
