using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mw.player;


public class Enemy : Character
{
    
    EnemyController enemyController;    
    public Estados tipoDeComportamiento;

    [System.Serializable]
    public enum Estados
    {
        Static,
        Melee,
        Death,
    }
    

    public void Initialized()
    {
        if (enemyController != null)
        {
            return;
        }
        enemyController = GetComponent<EnemyController>();
    }

    //Esto se est� llamando desde el Character State "EnemyActiveState"
    public void Comportamiento()
    {
        switch (tipoDeComportamiento)
        {
            case Estados.Static:  
                enemyController.canMove = false;
                break;
            case Estados.Melee:
                enemyController.canMove = true;
                break;
            case Estados.Death:
                enemyController.isAlive = false;
                break;

        }
        
    }

}
