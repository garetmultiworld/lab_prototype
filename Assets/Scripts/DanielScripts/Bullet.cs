using UnityEngine;

public class Bullet : MonoBehaviour
{
    public TriggerInterface OnHitTrigger;
    
    public LayerMask FilterLayer;
    private Transform target;
    public float speed = 10f;
    public float damageAmount = 1;
    Vector3 dir;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    private void Start()
    {
        dir = target.position - transform.position;        
    }
    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        
        float distanceThisFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);

    }

    void HitTarget()
    {
        
        Destroy(gameObject);
        
    }

    public void OnTriggerEnter(Collider c)
    {
        Health health = c.GetComponent<Health>();
        if (health != null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            health.ReceiveDirectDamage(damageAmount, gameObject);
            if (OnHitTrigger != null)
            {
                OnHitTrigger.Fire();
            }
            HitTarget();
        }
    }
}
