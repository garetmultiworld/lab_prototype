using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGameObjectToListTrigger : TriggerInterface
{

#if UNITY_EDITOR
    
#endif

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public GameObject prefabToAdd;
    public CapsuleLaunchManager manager;

    public override void Cancel()
    { }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        manager.robotPrefab.Add(prefabToAdd);
    }
    /*
    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToCancel == null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " (" + TriggerToCancel.GetLabel() + ")";
        }
        return desc;
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("Trigger to cancel", TriggerToCancel, 0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerToCancel = trigger;
    }
#endif
*/
}