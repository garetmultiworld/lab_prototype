using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseOverTrigger : MonoBehaviour
{
    public TriggerInterface Trigger;

    private void OnMouseOver()
    {
        if (Trigger != null)
        {
            Trigger.Fire();
        }
    }
    private void OnMouseExit()
    {
        if (Trigger != null)
        {
            Trigger.Cancel();
        }
    }
}
