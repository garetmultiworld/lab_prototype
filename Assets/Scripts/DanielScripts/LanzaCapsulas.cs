using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mw.player;

public class LanzaCapsulas : MonoBehaviour
{
    [Header("Propiedades Lanza Capsulas")]

    public int maxCapsulas = 3;
    public float fuerza = 200f;
    float throwForce;
    public GameObject capsulePrefab;
    public Transform origen;
    public AK.Wwise.Event capsuleLouchSound;

    [Header("Propiedades Cursor")]

    public GameObject cursor;
    public LayerMask layer;

    public Camera cam;

    Player player;
    private void Start()
    {
        player = PlayerManager.Instance.GetPlayer(0);
    }
    // Update is called once per frame
    void Update()
    {
        if (maxCapsulas > 0)
        {
            PlayerInput();
        }
        else
        {
            if (Input.GetButtonDown("Lanzar"))
            {
                Debug.Log("No tienes m�s capsulas disponibles!");
            }            
        }
        
    }

    void PlayerInput()
    {
        if (Input.GetButtonDown("Lanzar"))
        {
            throwForce = fuerza;
        }
        if (Input.GetAxis("Lanzar")>0)
        {
            if (throwForce < 700f)
            {
                throwForce += 20f;
            }            
            CursorGuide();
        }
        if (Input.GetButtonUp("Lanzar"))
        {
            LanzarCapsula();
            cursor.SetActive(false);
        }
    }

    void LanzarCapsula()
    {
        capsuleLouchSound.Post(gameObject);
        GameObject capsula = Instantiate(capsulePrefab, origen.position, origen.rotation);
        Rigidbody rb = capsula.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * throwForce);
        maxCapsulas -= 1;

        Debug.Log(throwForce);        
    }

    void CursorGuide()
    {
        Ray camRay = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(camRay, out hit, 100f, layer))
        {
            cursor.SetActive(true);
            cursor.transform.position = hit.point + Vector3.up * 0.1f;

            player.transform.LookAt(cursor.transform.position);
        }

        
    }
}
