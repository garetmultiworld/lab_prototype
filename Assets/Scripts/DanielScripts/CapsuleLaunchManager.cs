using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleLaunchManager : MonoBehaviour
{
    //Asignar ese objeto al LanzaCapsulas
    public LanzaCapsulas capsuleLauncher;
    public DraggableInventorySlot[] quickSlots;

    [HideInInspector]
    //Revisar con el input del scroll a trav�s de una List
    public List<GameObject> robotPrefab;
    GameObject currentEquip;

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
    }

    void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AddRobottoLauncher(robotPrefab[0]);
            if(currentEquip != null)
            {
                Destroy(currentEquip);
            }
            currentEquip = Instantiate(quickSlots[0].inventorySource.GetItemUIPrefab(quickSlots[0].Name), transform);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            AddRobottoLauncher(robotPrefab[1]);
            if (currentEquip != null)
            {
                Destroy(currentEquip);
            }
            currentEquip = Instantiate(quickSlots[1].inventorySource.GetItemUIPrefab(quickSlots[1].Name), transform);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            AddRobottoLauncher(robotPrefab[2]);
            if (currentEquip != null)
            {
                Destroy(currentEquip);
            }
            currentEquip = Instantiate(quickSlots[2].inventorySource.GetItemUIPrefab(quickSlots[2].Name), transform);
        }
    }

    void AddRobottoLauncher(GameObject robot)
    {
        capsuleLauncher.capsulePrefab = robot;
    }
}
