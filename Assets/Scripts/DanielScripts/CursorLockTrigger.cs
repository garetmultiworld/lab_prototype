﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorLockTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool LockCursor = false;
    public bool showCursor = false;
    public Locking_Camera mouseCam;
    bool changeMouse = false;
#endif
    private void Start()
    {
        if (mouseCam != null && !mouseCam.activate)
            {                
                changeMouse = true;
            }
    }
    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public override void Cancel()
    {}

    public override void Fire()
    {
        
        if (!CanTrigger())
        {
            return;
        }        
        if (LockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            if (changeMouse)
            {
                mouseCam.activate = false;
            }
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            if (changeMouse)
            {
                mouseCam.activate = true;
            }
        }

        if (showCursor)
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
    }

    /*public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToCancel==null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " ("+TriggerToCancel.GetLabel()+")";
        }
        return desc;
    }*/

#if UNITY_EDITOR
    /*public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("Trigger to cancel", TriggerToCancel, 0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerToCancel = trigger;
    }*/
#endif

}
