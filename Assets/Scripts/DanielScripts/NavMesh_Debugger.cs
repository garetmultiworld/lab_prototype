using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMesh_Debugger : MonoBehaviour
{
    [SerializeField]
    private NavMeshAgent agentToDebug;
    private LineRenderer LineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        LineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(agentToDebug.hasPath)
        {
            LineRenderer.positionCount = agentToDebug.path.corners.Length;
            LineRenderer.SetPositions(agentToDebug.path.corners);
            LineRenderer.enabled = true;
        }
        else
        {
            LineRenderer.enabled = false;
        }
    }
}
