using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{    
    public DamageOnTouch weapon;

    public void BeginAttack()
    {
        weapon.canAttack = true;
    }

    public void EndAttack()
    {
        weapon.canAttack = false;
    }

}
