using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InstantiateTrigger : TriggerInterface
{

#if UNITY_EDITOR
    
#endif

    public static string[] TriggerCategories = { "Trigger", "Logic" };

    public GameObject prefabToInstantiate;
    public Transform parent;

    public override void Cancel()
    { }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if(parent != null)
        {
            Instantiate(prefabToInstantiate, parent);
        }
        else
        {
            Instantiate(prefabToInstantiate);
        }
        
        
    }

}
