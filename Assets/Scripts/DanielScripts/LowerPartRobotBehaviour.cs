using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LowerPartRobotBehaviour : MonoBehaviour
{    
    Transform target;
    NavMeshAgent agent;
    public float speed = 20f;
    public float radius = 10f;
    public Estados tipoDeComportamiento;
    bool isStatic = false;


    [System.Serializable]
    public enum Estados
    {
        Static,
        LandPatrol,
        AirPatrol,
    }

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerTag.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = radius;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isStatic)
        {
            Comportamiento();
        }
        
    }

    void Comportamiento()
    {
        switch (tipoDeComportamiento)
        {
            case Estados.Static:
                //Aqui se pone como se comporta en static
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.isKinematic = true;
                isStatic = true;
                break;
            case Estados.LandPatrol:
                LandPatrol();
                break;
            case Estados.AirPatrol:
                //La patrulla aerea
                break;

        }
    }

    void LandPatrol()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance > radius)
        {
            agent.SetDestination(target.position);
        }
        else
        {
            transform.RotateAround(target.transform.position, Vector3.up, speed * Time.deltaTime);
        }
    }

}
