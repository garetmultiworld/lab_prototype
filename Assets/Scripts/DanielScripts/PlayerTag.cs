using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTag : MonoBehaviour
{
    #region Singleton

    public static PlayerTag instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject player;
}
