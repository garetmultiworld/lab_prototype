using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsula : MonoBehaviour
{
    //public float delay = 3f;

    public GameObject explosionEffect;
    public GameObject robotType;
    public AK.Wwise.Event CapsuleExplosionSound;

    /*float countdown;
    bool hasExploded = false;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;

        if(countdown <= 0 && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }*/

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }

    void Explode()
    {
        //show effect
        Instantiate(explosionEffect, transform.position, transform.rotation);
        //Invoke robot
        Instantiate(robotType, transform.position, transform.rotation);
        //Destroy
        Destroy(gameObject);
        //ExplosionSound
        CapsuleExplosionSound.Post(gameObject);
    }
}
