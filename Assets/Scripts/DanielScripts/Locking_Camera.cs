using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Locking_Camera : MonoBehaviour, AxisState.IInputAxisProvider
{
    public string HorizontalInput = "Mouse X";
    public string VerticalInput = "Mouse Y";
    public bool activate = true;
    public float GetAxisValue(int axis)
    {        
        // Revisa que el Lock Cursor sea verdadero
        if (!Input.GetMouseButton(1) && activate)
            return 0;

        switch (axis)
        {
            case 0: return Input.GetAxis(HorizontalInput);
            case 1: return Input.GetAxis(VerticalInput);
            default: return 0;
        }
    }
}
