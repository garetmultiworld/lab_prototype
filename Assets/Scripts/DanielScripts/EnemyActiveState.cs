using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mw.player;

public class EnemyActiveState : CharacterState
{    
    Enemy thisEnemy;    
    
    public override void OnEnterState()
    {
        thisEnemy = GetComponent<Enemy>();
        thisEnemy.Initialized();
        thisEnemy.Comportamiento();
    }

    public override void OnExitState()
    {
        thisEnemy.tipoDeComportamiento = Enemy.Estados.Death;
        thisEnemy.Comportamiento();        
    }

    public override void OnFixedUpdate()
    {
        
    }

    public override void OnLateUpdate()
    {
        
    }

    public override void OnPauseState()
    {
        
    }

    public override void OnResumeState()
    {
        
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
        
    }

    public override void OnUpdate()
    {       
        
    }

}
