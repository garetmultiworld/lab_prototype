﻿using mw.player;
using System.Collections;
using UnityEngine;

public class BehaviorFollowPlayer : MonoBehaviour
{

    public TriggerInterface OnStartFollow;
    public TriggerInterface OnStopFollow;
    public float MovementSpeed=1f;
    public float MinDistanceToGoal = .1f;
    public bool StopOnArrive=false;
    public TriggerInterface OnArrive;

    private Rigidbody2D body;

    protected bool _following = false;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    public void StopFollowing(bool withoutCallback=false)
    {
        if (!_following)
        {
            return;
        }
        _following = false;
        if (OnStopFollow != null&&!withoutCallback)
        {
            OnStopFollow.Fire();
        }
    }

    public void StartFollowing()
    {
        if (_following)
        {
            return;
        }
        _following = true;
        if (OnStartFollow!= null)
        {
            OnStartFollow.Fire();
        }
        StartCoroutine(FollowPlayer());
    }

    public void SignalArrived()
    {
        if (!_following)
        {
            return;
        }
        _following = false;
        if (OnArrive != null)
        {
            OnArrive.Fire();
        }
    }

    public bool Arrived(Vector3 position)
    {
        Vector3 currentPos = transform.position;
        float _distanceToNextPoint = (position - currentPos).magnitude;
        return _distanceToNextPoint < MinDistanceToGoal;
    }

    private IEnumerator FollowPlayer()
    {
        while (_following)
        {
            Vector3 TargetPos = PlayerManager.Instance.GetPlayer(0).transform.position;
            if (StopOnArrive&&Arrived(TargetPos))
            {
                SignalArrived();
            }
            else
            {
                //use transform.right if 3D
                //transform.up= Vector3.Lerp(transform.up, (target.position - transform.position), turnRate);
                transform.up = TargetPos - transform.position;
                Vector3 Direction = (TargetPos - transform.position).normalized;
                body.MovePosition(new Vector2(
                    (transform.position.x + Direction.x * MovementSpeed * Time.deltaTime),
                    transform.position.y + Direction.y * MovementSpeed * Time.deltaTime)
                );
                yield return null;
            }
        }
    }

}
