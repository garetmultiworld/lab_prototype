﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class EditorUtils
{

    public static void DrawUILine(Color color, int thickness = 1, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }

    public static LayerMask SelectLayerMask(EditorBase editor, string Label,LayerMask layerMask)
    {
        LayerMask prev= layerMask;
        if (layerMask == 0)
        {
            EditorGUILayout.HelpBox("No layer Selected", MessageType.Warning);
        }
        GUILayout.Label(Label);
        LayerMask tempMask = EditorGUILayout.MaskField(
            InternalEditorUtility.LayerMaskToConcatenatedLayersMask(layerMask),
            InternalEditorUtility.layers
        );
        layerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(tempMask);
        if (prev != layerMask)
        {
            editor.HadChanges = true;
        }
        return layerMask;
    }

    public static bool Checkbox(EditorBase editor,string Label,bool Value) {
        bool prev = Value;
        GUILayout.BeginHorizontal();
        GUILayout.Label(Label);
        Value = EditorGUILayout.Toggle(Value);
        GUILayout.EndHorizontal();
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static bool Toggle(EditorBase editor, string Label, bool Value)
    {
        bool prev = Value;
        Value = EditorGUILayout.Toggle(Label,Value);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static string TextField(EditorBase editor,string Label,string Value)
    {
        string prev = Value;
        Value = EditorGUILayout.TextField(Label, Value);
        if (
                (string.IsNullOrEmpty(prev) && !string.IsNullOrEmpty(Value)) ||
                (!string.IsNullOrEmpty(prev) && string.IsNullOrEmpty(Value)) ||
                (prev != null && !prev.Equals(Value))
            )
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static float FloatField(EditorBase editor, string label, float Value)
    {
        return FloatField(editor, new GUIContent(label), Value);
    }

    public static float FloatField(EditorBase editor, GUIContent label, float Value)
    {
        float prev = Value;
        Value = EditorGUILayout.FloatField(label, Value);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static Vector2 Vector2Field(EditorBase editor,string label,Vector2 Value)
    {
        Vector2 prev = Value;
        Value = EditorGUILayout.Vector2Field(label,Value);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static Vector3 Vector3Field(EditorBase editor, string label, Vector3 Value)
    {
        Vector3 prev = Value;
        Value = EditorGUILayout.Vector2Field(label, Value);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static float[] MinMaxField(EditorBase editor, string label, float Min, float Max)
    {
        float prevMin = Min;
        float prevMax = Max;
        GUILayout.BeginHorizontal();
        GUILayout.Label(label);
        GUILayout.Label("Min");
        Min = EditorGUILayout.FloatField(Min);
        GUILayout.Label("Max");
        Max = EditorGUILayout.FloatField(Max);
        GUILayout.EndHorizontal();
        if (Min != prevMin || Max != prevMax)
        {
            editor.HadChanges = true;
        }
        float[] result = new float[] { Min, Max };
        return result;
    }

    public static int IntField(EditorBase editor, string label, int Value)
    {
        return IntField(editor,new GUIContent(label), Value);
    }

    public static int IntField(EditorBase editor,GUIContent label,int Value)
    {
        int prev = Value;
        Value = EditorGUILayout.IntField(label, Value);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static float Slider(EditorBase editor, string label, float Value, float min, float max)
    {
        float prev = Value;
        Value = EditorGUILayout.Slider(label, Value, min, max);
        if (Value != prev)
        {
            editor.HadChanges = true;
        }
        return Value;
    }

    public static TriggerInterface TriggerField(EditorBase editor, TriggerInterface trigger)
    {
        TriggerInterface prev = trigger;
        trigger = (TriggerInterface)EditorGUILayout.ObjectField(
            trigger,
            typeof(TriggerInterface),
            true
        );
        if (trigger != prev)
        {
            editor.HadChanges = true;
        }
        return trigger;
    }

    public static System.Enum EnumPopup(EditorBase editor, System.Enum selected)
    {
        return EnumPopup(editor, "", selected);
    }

    public static System.Enum EnumPopup(EditorBase editor, string label, System.Enum selected)
    {
        System.Enum prev = selected;
        selected =EditorGUILayout.EnumPopup(label, selected);
        if (prev != selected)
        {
            editor.HadChanges = true;
        }
        return selected;
    }

    public static Inventory InventoryField(EditorBase editor, string label, Inventory prefab)
    {
        Inventory prev = prefab;
        prefab = (Inventory)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Inventory),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static RectBounds RectBoundsField(EditorBase editor, string label, RectBounds prefab)
    {
        RectBounds prev = prefab;
        prefab = (RectBounds)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(RectBounds),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static Rigidbody RigidbodyField(EditorBase editor, string label, Rigidbody prefab)
    {
        Rigidbody prev = prefab;
        prefab = (Rigidbody)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Rigidbody),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static Rigidbody2D Rigidbody2DField(EditorBase editor, string label, Rigidbody2D prefab)
    {
        Rigidbody2D prev = prefab;
        prefab = (Rigidbody2D)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Rigidbody2D),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static Text TextField(EditorBase editor, string label, Text prefab)
    {
        Text prev = prefab;
        prefab = (Text)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Text),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static I18nManager I18nManagerField(EditorBase editor, string label, I18nManager prefab)
    {
        I18nManager prev = prefab;
        prefab = (I18nManager)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(I18nManager),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static GameObject GameObjectField(EditorBase editor, GameObject prefab)
    {
        GameObject prev = prefab;
        prefab = (GameObject)EditorGUILayout.ObjectField(
            prefab,
            typeof(GameObject),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static GameObject GameObjectField(EditorBase editor, string label, GameObject prefab)
    {
        GameObject prev = prefab;
        prefab = (GameObject)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(GameObject),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static Transform TransformField(EditorBase editor, string label, Transform prefab)
    {
        Transform prev = prefab;
        prefab = (Transform)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Transform),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static Animator AnimatorField(EditorBase editor, string label, Animator prefab)
    {
        Animator prev = prefab;
        prefab = (Animator)EditorGUILayout.ObjectField(
            label,
            prefab,
            typeof(Animator),
            true
        );
        if (prefab != prev)
        {
            editor.HadChanges = true;
        }
        return prefab;
    }

    public static AnimationCurve AnimationCurveField(EditorBase editor, string label, AnimationCurve prefab)
    {
        AnimationCurve prev = prefab;
        prefab = EditorGUILayout.CurveField(label, prefab);
        editor.HadChanges = true;
        return prefab;
    }

    public static TargetHolder TargetHolderField(EditorBase editor,string label, TargetHolder targetHolder)
    {
        TargetHolder prev = targetHolder;
        targetHolder = (TargetHolder)EditorGUILayout.ObjectField(
            label,
            targetHolder,
            typeof(TargetHolder),
            true
        );
        if (targetHolder != prev)
        {
            editor.HadChanges = true;
        }
        return targetHolder;
    }

    public static void TriggerDescription(TriggerInterface trigger)
    {
        if (trigger != null)
        {
            string desc = trigger.GetDescription();
            if (string.IsNullOrEmpty(desc))
            {
                desc = "(trigger has no description)";
            }
            EditorGUILayout.HelpBox(desc, MessageType.None);
        }
    }

    public static TriggerInterface SelectTrigger(EditorBase editor, string Label,TriggerInterface trigger)
    {
        TriggerInterface prev = trigger;
        if (trigger != null)
        {
            DrawUILine(Color.grey);
        }
        trigger = (TriggerInterface)EditorGUILayout.ObjectField(
            Label,
            trigger,
            typeof(TriggerInterface),
            true
        );
        if (trigger != null)
        {
            TriggerDescription(trigger);
            DrawUILine(Color.grey);
        }
        if (trigger != prev)
        {
            editor.HadChanges = true;
        }
        return trigger;
    }

}
