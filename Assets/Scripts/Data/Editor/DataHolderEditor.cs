﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DataHolder))]
public class DataHolderEditor : EditorBase
{

    protected void SetUpPrefabConflict(DataHolder trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "DataHolder");
    }

    protected void StorePrefabConflict(DataHolder trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        DataHolder dataHolder = (DataHolder)target;

        SetUpPrefabConflict(dataHolder);

        dataHolder.showDataHolder = EditorGUILayout.Foldout(
            dataHolder.showDataHolder,
            "Data Holder (" + dataHolder.Name+")"
        );
        if (dataHolder.showDataHolder)
        {

            dataHolder.DebugData = EditorUtils.Checkbox(this, "Debug", dataHolder.DebugData);

            dataHolder.Name = EditorUtils.TextField(this, "Name", dataHolder.Name);

            dataHolder.type = (DataHolder.Type)EditorUtils.EnumPopup(this, dataHolder.type);

            dataHolder.OnChangeTrigger = EditorUtils.SelectTrigger(this, "On Change", dataHolder.OnChangeTrigger);

            switch (dataHolder.type)
            {
                case DataHolder.Type.Bool:
                    bool prevBool = EditorUtils.Checkbox(this, "Default Value", dataHolder.BoolValue);
                    if (_hadChanges)
                    {
                        dataHolder.SetValue(prevBool);
                    }
                    break;
                case DataHolder.Type.Number:
                    float prevFloat = EditorUtils.FloatField(this, "Default Value", dataHolder.FloatValue);
                    if (_hadChanges)
                    {
                        dataHolder.SetValue(prevFloat);
                    }
                    break;
                case DataHolder.Type.Integer:
                    int prevInt = EditorUtils.IntField(this, "Default Value", dataHolder.IntValue);
                    if (_hadChanges)
                    {
                        dataHolder.SetValue(prevInt);
                    }
                    break;
                case DataHolder.Type.Text:
                    string prevString = EditorUtils.TextField(this, "Default Value", dataHolder.Value);
                    if (_hadChanges)
                    {
                        dataHolder.SetValue(prevString);
                    }
                    break;
            }
        }

        StorePrefabConflict(dataHolder);
    }

}
