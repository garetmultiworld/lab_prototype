﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SetDataFromDataTrigger))]
public class SetDataFromDataTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(SetDataFromDataTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "SetDataFromDataTrigger");
    }

    protected void StorePrefabConflict(SetDataFromDataTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SetDataFromDataTrigger setDataFromDataTrigger = (SetDataFromDataTrigger)target;

        SetUpPrefabConflict(setDataFromDataTrigger);

        setDataFromDataTrigger.showSetDataFromDataTrigger = EditorGUILayout.Foldout(
            setDataFromDataTrigger.showSetDataFromDataTrigger,
            "Set Data"
        );
        if (setDataFromDataTrigger.showSetDataFromDataTrigger)
        {

            setDataFromDataTrigger.Holder = EditorUtils.GameObjectField(this, "Holder", setDataFromDataTrigger.Holder);
            setDataFromDataTrigger.LabelData = EditorUtils.TextField(this, "Data Name", setDataFromDataTrigger.LabelData);
            setDataFromDataTrigger.FromTarget = EditorUtils.Checkbox(this, "Is In Target Holder", setDataFromDataTrigger.FromTarget);

            EditorUtils.DrawUILine(Color.gray);

            setDataFromDataTrigger.HolderDestination = EditorUtils.GameObjectField(this, "Destination Holder", setDataFromDataTrigger.HolderDestination);
            setDataFromDataTrigger.LabelDataDestination = EditorUtils.TextField(this, "Destination Data Name", setDataFromDataTrigger.LabelDataDestination);
            setDataFromDataTrigger.FromTargetDestination = EditorUtils.Checkbox(this, "Destination Is In Target Holder", setDataFromDataTrigger.FromTargetDestination);
        }

        StorePrefabConflict(setDataFromDataTrigger);
    }
}
