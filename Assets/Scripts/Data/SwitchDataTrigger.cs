﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDataTrigger : TriggerInterface
{

    public DataHolder dataHolder;
    public SwitchDataCaseTrigger[] Cases;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (SwitchDataCaseTrigger theCase in Cases)
        {
            if (theCase != null && theCase.DataCoincides(dataHolder) && theCase.Trigger != null)
            {
                theCase.Trigger.Fire(this);
            }
        }
    }

}
