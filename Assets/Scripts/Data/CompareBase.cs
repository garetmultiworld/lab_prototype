﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CompareBase : TriggerInterface
{

    public enum Operation
    {
        Equals,
        LessThan,
        LessEqualThan,
        GreaterThan,
        GreaterEqualThan
    }

    abstract public void StartComparison();

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        StartComparison();
    }

    public static DataHolder GetDataFromTarget(GameObject parent, string label)
    {
        DataHolder result = null;
        DataHolder[] dataHolders = parent.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (label.Equals(data.Name))
            {
                result = data;
                break;
            }
        }
        return result;
    }

    protected DataHolder GetData(GameObject parent, string label)
    {
        if (parent != null)
        {
            return CompareBase.GetDataFromTarget(parent,label);
        }
        else
        {
            return CompareBase.GetDataFromTarget(this.gameObject, label);
        }
    }

    public void Compare(DataHolder a, DataHolder b, Operation operation, TriggerInterface ifTrue, TriggerInterface ifFalse)
    {
        switch (operation)
        {
            case Operation.Equals:
                CheckResult(IsEqual(a, b), ifTrue, ifFalse);
                break;
            case Operation.GreaterThan:
                CheckResult(IsGreaterThan(a, b), ifTrue, ifFalse);
                break;
            case Operation.GreaterEqualThan:
                CheckResult(IsGreaterEqualThan(a, b), ifTrue, ifFalse);
                break;
            case Operation.LessThan:
                CheckResult(IsLessThan(a, b), ifTrue, ifFalse);
                break;
            case Operation.LessEqualThan:
                CheckResult(IsLessEqualThan(a, b), ifTrue, ifFalse);
                break;
        }
    }

    public void CheckResult(bool result, TriggerInterface ifTrue, TriggerInterface ifFalse)
    {
        if (result)
        {
            if (ifTrue != null)
            {
                ifTrue.Fire(this);
            }
        }
        else
        {
            if (ifFalse != null)
            {
                ifFalse.Fire(this);
            }
        }
    }

    public bool IsEqual(DataHolder a, DataHolder b)
    {
        bool result;
        switch (a.type)
        {
            default://assumes string
                result = a.Value.Equals(b.Value);
                break;
            case DataHolder.Type.Number:
                result = a.FloatValue==b.FloatValue;
                break;
            case DataHolder.Type.Integer:
                result = a.IntValue == b.IntValue;
                break;
            case DataHolder.Type.Bool:
                result = a.BoolValue == b.BoolValue;
                break;
        }
        return result;
    }

    public bool IsGreaterThan(DataHolder a, DataHolder b)
    {
        bool result;
        switch (a.type)
        {
            default://assumes number
                result = a.FloatValue>b.FloatValue;
                break;
            case DataHolder.Type.Integer:
                result = a.IntValue > b.IntValue;
                break;
            case DataHolder.Type.Text:
                result = a.Value.CompareTo(b.Value)>0;
                break;
        }
        return result;
    }

    public bool IsGreaterEqualThan(DataHolder a, DataHolder b)
    {
        bool result;
        switch (a.type)
        {
            default://assumes number
                result = a.FloatValue >= b.FloatValue;
                break;
            case DataHolder.Type.Integer:
                result = a.IntValue >= b.IntValue;
                break;
            case DataHolder.Type.Text:
                result = a.Value.CompareTo(b.Value) >= 0;
                break;
        }
        return result;
    }

    public bool IsLessThan(DataHolder a, DataHolder b)
    {
        bool result;
        switch (a.type)
        {
            default://assumes number
                result = a.FloatValue < b.FloatValue;
                break;
            case DataHolder.Type.Integer:
                result = a.IntValue < b.IntValue;
                break;
            case DataHolder.Type.Text:
                result = a.Value.CompareTo(b.Value) < 0;
                break;
        }
        return result;
    }

    public bool IsLessEqualThan(DataHolder a, DataHolder b)
    {
        bool result;
        switch (a.type)
        {
            default://assumes number
                result = a.FloatValue <= b.FloatValue;
                break;
            case DataHolder.Type.Integer:
                result = a.IntValue <= b.IntValue;
                break;
            case DataHolder.Type.Text:
                result = a.Value.CompareTo(b.Value) <= 0;
                break;
        }
        return result;
    }

}
