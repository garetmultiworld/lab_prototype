﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperateConstant : OperateBase
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showOperateConstant=true;
#endif

    public GameObject Holder;
    public string LabelData;
    public bool DataFromTarget;
    public bool DataOnLeft = true;
    public Operation operation;
    public Type ConstantType;
    public string StringValue;
    public float NumberValue;
    public int IntegerValue;

    protected DataHolder TempHolder;

    void Awake()
    {
        TempHolder = gameObject.AddComponent<DataHolder>();
    }

    public override float FloatValue
    {
        get
        {
            DataHolder DynamicData;
#if UNITY_EDITOR
            if (Application.isEditor && !Application.isPlaying)
            {
                return InternalFloatValue;
            }
#endif
            TempHolder.type = ConstantType;
            TempHolder.SetValue(StringValue);
            TempHolder.SetValue(NumberValue);
            TempHolder.SetValue(IntegerValue);
            if (DataFromTarget)
            {
                DynamicData = GetData(Holder.GetComponent<TargetHolder>().Target, LabelData);
            }
            else
            {
                DynamicData = GetData(Holder, LabelData);
            }
            if (DynamicData == null)
            {
                return InternalFloatValue;
            }
            if (DataOnLeft)
            {
                Operate(DynamicData, TempHolder, operation);
            }
            else
            {
                Operate(TempHolder, DynamicData, operation);
            }
            return InternalFloatValue;
        }
    }

    public override int IntValue
    {
        get
        {
            DataHolder DynamicData;
#if UNITY_EDITOR
            if (Application.isEditor && !Application.isPlaying)
            {
                return InternalIntValue;
            }
#endif
            TempHolder.type = ConstantType;
            TempHolder.SetValue(StringValue);
            TempHolder.SetValue(NumberValue);
            TempHolder.SetValue(IntegerValue);
            if (DataFromTarget)
            {
                DynamicData = GetData(Holder.GetComponent<TargetHolder>().Target, LabelData);
            }
            else
            {
                DynamicData = GetData(Holder, LabelData);
            }
            if (DynamicData == null)
            {
                return InternalIntValue;
            }
            if (DataOnLeft)
            {
                Operate(DynamicData, TempHolder, operation);
            }
            else
            {
                Operate(TempHolder, DynamicData, operation);
            }
            return InternalIntValue;
        }
    }

}
