﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDataFromTriggerParameter : TriggerInterface
{

    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        data.SetValue(StringParameter);
                        break;
                    case DataHolder.Type.Number:
                        data.SetValue(FloatParameter);
                        break;
                    case DataHolder.Type.Integer:
                        data.SetValue((int)IntParameter);
                        break;
                    case DataHolder.Type.Bool:
                        //not supported
                        break;
                }
                break;
            }
        }
    }

}
