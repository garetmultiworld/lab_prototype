﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareDataTrigger : CompareBase
{

    public GameObject Left;
    public string LabelDataLeft;
    public bool DataFromTargetLeft;
    public Operation operation;
    public GameObject Right;
    public string LabelDataRight;
    public bool DataFromTargetRight;

    public TriggerInterface IfTrue;
    public TriggerInterface IfFalse;

    public override void StartComparison()
    {
        DataHolder LeftData;
        if (DataFromTargetLeft)
        {
            LeftData = GetData(Left.GetComponent<TargetHolder>().Target, LabelDataLeft);
        }
        else
        {
            LeftData = GetData(Left, LabelDataLeft);
        }
        DataHolder RightData;
        if (DataFromTargetRight)
        {
            RightData = GetData(Right.GetComponent<TargetHolder>().Target, LabelDataRight);
        }
        else
        {
            RightData = GetData(Right, LabelDataRight);
        }
        if (LeftData == null || RightData == null)
        {
            return;
        }
        Compare(LeftData,RightData,operation,IfTrue,IfFalse);
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("True", IfTrue,0);
        callbacks[1] = new TriggerCallback("False", IfFalse,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            IfTrue = trigger;
        }
        else
        {
            IfFalse = trigger;
        }
    }
#endif

}
