﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperateData : OperateBase
{

    public GameObject Left;
    public string LabelDataLeft;
    public bool DataFromTargetLeft;
    public Operation operation;
    public GameObject Right;
    public string LabelDataRight;
    public bool DataFromTargetRight;

    public override float FloatValue
    {
        get
        {
            DataHolder LeftData;
            if (DataFromTargetLeft)
            {
                LeftData = GetData(Left.GetComponent<TargetHolder>().Target, LabelDataLeft);
            }
            else
            {
                LeftData = GetData(Left, LabelDataLeft);
            }
            DataHolder RightData;
            if (DataFromTargetRight)
            {
                RightData = GetData(Right.GetComponent<TargetHolder>().Target, LabelDataRight);
            }
            else
            {
                RightData = GetData(Right, LabelDataRight);
            }
            if (LeftData == null || RightData == null)
            {
                return InternalFloatValue;
            }
            Operate(LeftData, RightData, operation);
            return InternalFloatValue;
        }
    }

    public override int IntValue
    {
        get
        {
            DataHolder LeftData;
            if (DataFromTargetLeft)
            {
                LeftData = GetData(Left.GetComponent<TargetHolder>().Target, LabelDataLeft);
            }
            else
            {
                LeftData = GetData(Left, LabelDataLeft);
            }
            DataHolder RightData;
            if (DataFromTargetRight)
            {
                RightData = GetData(Right.GetComponent<TargetHolder>().Target, LabelDataRight);
            }
            else
            {
                RightData = GetData(Right, LabelDataRight);
            }
            if (LeftData == null || RightData == null)
            {
                return InternalIntValue;
            }
            Operate(LeftData, RightData, operation);
            return InternalIntValue;
        }
    }

}
