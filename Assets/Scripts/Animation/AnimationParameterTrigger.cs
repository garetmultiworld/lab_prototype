﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationParameterTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showAnimationParameterTrigger = true;
#endif

    public static string[] TriggerCategories = { "Trigger", "Animation" };

    [System.Serializable]
    public enum ParamTypes
    {
        Float,
        Integer,
        Bool,
        Trigger
    }

    public Animator Animator;
    public ParamTypes Type;
    public string ParameterName;
    public float FloatValue;
    public int IntegerValue;
    public bool BoolValue;

    public override void Cancel()
    {}

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Animator==null)
        {
            Debug.Log("animator not defined! ("+GetLabel()+")");
            return;
        }
        switch (Type)
        {
            case ParamTypes.Float:
                Animator.SetFloat(ParameterName, FloatValue);
                break;
            case ParamTypes.Integer:
                Animator.SetInteger(ParameterName, IntegerValue);
                break;
            case ParamTypes.Bool:
                Animator.SetBool(ParameterName, BoolValue);
                break;
            case ParamTypes.Trigger:
                Animator.SetTrigger(ParameterName);
                break;
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (";
        if (Animator==null)
        {
            desc += "no animator";
        }
        else
        {
            desc += Animator.gameObject.name;
        }
        if (string.IsNullOrEmpty(ParameterName))
        {
            desc += ", no parameter";
        }
        else
        {
            desc += ", " + ParameterName;
        }
        switch (Type)
        {
            case ParamTypes.Float:
                desc += ", Float, "+FloatValue.ToString();
                break;
            case ParamTypes.Integer:
                desc += ", Integer, " + FloatValue.ToString();
                break;
            case ParamTypes.Bool:
                desc += ", Bool, " + (BoolValue?"true":"false");
                break;
            case ParamTypes.Trigger:
                desc += ", Trigger";
                break;
        }
        desc += ")";
        return desc;
    }

}
