﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMultipleObjectTrigger : TriggerInterface
{
    public GameObject[] Objects = new GameObject[0];
    public bool OnlyTarget;

    public override void Cancel()
    {
        foreach (GameObject gameObject in Objects)
        {
            gameObject.SetActive(true);
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (GameObject gameObject in Objects)
        {
            if (gameObject != null)
            {
                if (OnlyTarget)
                {
                    Destroy(gameObject.GetComponent<TargetHolder>().Target);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }

}
