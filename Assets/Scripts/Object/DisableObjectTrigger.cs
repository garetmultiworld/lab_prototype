﻿using UnityEngine;

public class DisableObjectTrigger : TriggerInterface
{

    public static string[] TriggerCategories = { "Trigger", "Object" };

    public GameObject TheObject;

    public override void Cancel()
    {
        TheObject.SetActive(true);
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheObject.SetActive(false);
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + "(";
        if (TheObject != null)
        {
            desc += TheObject.name;
        }
        else
        {
            desc += "No object assigned";
        }
        desc += ")";
        return desc;
    }

}
