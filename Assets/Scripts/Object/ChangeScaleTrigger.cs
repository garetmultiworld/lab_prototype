﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScaleTrigger : TriggerInterface
{

    public Vector3 NewScale;
    public GameObject Object;
    public bool Animate;
    public float MinTime = 1;
    public float MaxTime = 2;
    public AnimationCurve AnimCurveX;
    public AnimationCurve AnimCurveY;
    public TriggerInterface TriggerOnArrive;

    public bool DataFromTarget;

    private bool IsAnimating = false;
    private Vector3 StartingScale;
    private Vector3 TargetScale;
    private float TotalTime;
    private float CurrentTimer = 0;
    private GameObject TheObject;
    private float StartingXScale;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (DataFromTarget)
        {
            TheObject = Object.GetComponent<TargetHolder>().Target;
        }
        else
        {
            TheObject = Object;
        }
        TargetScale = NewScale;
        if (Animate)
        {
            StartingScale = TheObject.transform.localScale;
            TotalTime = UnityEngine.Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
            StartCoroutine(ChangeScale());
        }
        else
        {
            TheObject.transform.localScale = TargetScale;
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Fire(this);
            }
        }
    }

    private IEnumerator ChangeScale()
    {
        while (IsAnimating)
        {
            float t = CurrentTimer / TotalTime;
            Vector3 newScale = new Vector3(
                StartingScale.x + ((TargetScale.x - StartingScale.x) * AnimCurveX.Evaluate(t)),
                StartingScale.y + ((TargetScale.y - StartingScale.y) * AnimCurveY.Evaluate(t)),
                TheObject.transform.localScale.z
            );
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer >= TotalTime)
            {
                newScale.x = TargetScale.x;
                newScale.y = TargetScale.y;
                IsAnimating = false;
                if (TriggerOnArrive != null)
                {
                    TriggerOnArrive.Fire(this);
                }
            }
            TheObject.transform.localScale = newScale;
            yield return null;
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Arrive", TriggerOnArrive,0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerOnArrive = trigger;
    }
#endif

}
