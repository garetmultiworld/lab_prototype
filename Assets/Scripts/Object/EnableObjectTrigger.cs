﻿using UnityEngine;

public class EnableObjectTrigger : TriggerInterface
{

    public GameObject TheObject;

    public override void Cancel()
    {
        TheObject.SetActive(false);
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            TheObject.SetActive(true);
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + "(";
        if (TheObject!=null)
        {
            desc += TheObject.name;
        }
        else
        {
            desc += "No object assigned";
        }
        desc+=")";
        return desc;
    }

}
