﻿using UnityEngine;

public class EnableNextSequenceTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showEnableNextSequenceTrigger = true;
    public bool showEnableNextSequenceTriggerObjects = true;
#endif

    public bool loop;
    public TriggerInterface OnBegin;
    public TriggerInterface OnEnd;
    public GameObject[] Objects = new GameObject[0];

    protected int index = -1;

    public override void Cancel()
    {
    }

    public void SetIndex(int newIndex)
    {
        index = newIndex;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        FireNext();
    }

    public void FireNext()
    {
        if (index == -1&&OnBegin!=null)
        {
            OnBegin.Fire(this);
        }
        index++;
        if (index >= Objects.Length)
        {
            if (OnEnd != null)
            {
                OnEnd.Fire(this);
            }
            if (loop)
            {
                index = 0;
            }
            else
            {
                return;
            }
        }
        if (index == 0)
        {
            if (loop)
            {
                Objects[Objects.Length-1].SetActive(false);
            }
            Objects[index].SetActive(true);
        }
        else
        {
            Objects[index-1].SetActive(false);
            Objects[index].SetActive(true);
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("On Begin", OnBegin,0);
        callbacks[1] = new TriggerCallback("On End", OnEnd,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            OnBegin = trigger;
        }
        else
        {
            OnEnd = trigger;
        }
    }
#endif

}
