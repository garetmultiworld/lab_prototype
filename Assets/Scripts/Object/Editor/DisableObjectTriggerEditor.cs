﻿using UnityEditor;

[CustomEditor(typeof(DisableObjectTrigger))]
public class DisableObjectTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(DisableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "DisableObjectTrigger");
    }

    protected void StorePrefabConflict(DisableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DisableObjectTrigger disableObjectTrigger = (DisableObjectTrigger)target;
        SetUpPrefabConflict(disableObjectTrigger);
        if (disableObjectTrigger.TheObject==null)
        {
            EditorGUILayout.HelpBox("No object assigned", MessageType.Warning);
        }
        disableObjectTrigger.TheObject = EditorUtils.GameObjectField(
            this,
            "Object",
            disableObjectTrigger.TheObject
        );
        StorePrefabConflict(disableObjectTrigger);
    }
}
