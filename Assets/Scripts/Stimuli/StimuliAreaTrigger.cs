﻿using mw.player;
using UnityEngine;

public class StimuliAreaTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showStimuliAreaTrigger = true;
#endif

    public string Stimuli;
    public float Radius;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Radius);
        StimuliSensor Sensor;
        Character character;
        foreach (Collider hitCollider in hitColliders)
        {
            Sensor=hitCollider.gameObject.GetComponent<StimuliSensor>();
            if (Sensor != null)
            {
                Sensor.Stimuli(Stimuli,gameObject);
            }
            else
            {
                character = hitCollider.gameObject.GetComponent<Character>();
                if (character != null)
                {
                    character.Stimuli(Stimuli, gameObject);
                }
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Stimuli+", "+ Radius.ToString() + ")";
        return desc;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }

}
