﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StimuliTargetTrigger : TriggerInterface
{

    public string Stimuli;
    public StimuliSensor Sensor;
    public Character CharacterSensor;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Sensor != null)
        {
            Sensor.Stimuli(Stimuli, gameObject);
        }
        else if(CharacterSensor !=null)
        {
            CharacterSensor.Stimuli(Stimuli, gameObject);
        }
    }


}
