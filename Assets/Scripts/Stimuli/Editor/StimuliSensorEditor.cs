﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StimuliSensor), true)]
public class StimuliSensorEditor : EditorBase
{

    protected void SetUpPrefabConflict(StimuliSensor stimuliSensor)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(stimuliSensor, "StimuliSensor");
    }

    protected void StorePrefabConflict(StimuliSensor stimuliSensor)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(stimuliSensor);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(stimuliSensor.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        StimuliSensor stimuliSensor = (StimuliSensor)target;
        SetUpPrefabConflict(stimuliSensor);
        if (stimuliSensor.StimuliTriggers.Length == 0)
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
            {
                Array.Resize(ref stimuliSensor.StimuliTriggers, stimuliSensor.StimuliTriggers.Length + 1);
                _hadChanges = true;
            }
        }
        for (int iTrigger = 0; iTrigger < stimuliSensor.StimuliTriggers.Length; iTrigger++)
        {
            DrawItem(stimuliSensor, iTrigger);
        }
        StorePrefabConflict(stimuliSensor);
    }

    protected void DrawItem(StimuliSensor stimuliSensor, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref stimuliSensor.StimuliTriggers, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref stimuliSensor.StimuliTriggers, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref stimuliSensor.StimuliTriggers, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref stimuliSensor.StimuliTriggers, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (stimuliSensor.StimuliTriggers.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref stimuliSensor.StimuliTriggers, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(stimuliSensor, iTrigger);
        }
    }

    protected void DrawItemDetails(StimuliSensor stimuliSensor, int iTrigger)
    {
        GUILayout.Label(iTrigger.ToString()+": "+stimuliSensor.StimuliTriggers[iTrigger].Stimuli);
        stimuliSensor.StimuliTriggers[iTrigger].Stimuli = EditorUtils.TextField(
            this, 
            "Stimuli", 
            stimuliSensor.StimuliTriggers[iTrigger].Stimuli
        );
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Trigger");
        stimuliSensor.StimuliTriggers[iTrigger].Trigger = EditorUtils.TriggerField(
            this,
            stimuliSensor.StimuliTriggers[iTrigger].Trigger
        );
        EditorGUILayout.EndHorizontal();
        EditorUtils.TriggerDescription(stimuliSensor.StimuliTriggers[iTrigger].Trigger);
        if (iTrigger < (stimuliSensor.StimuliTriggers.Length - 1))
        {
            EditorUtils.DrawUILine(Color.grey);
        }
    }
}
