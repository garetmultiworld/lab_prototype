﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StimuliManager : MonoBehaviour
{
    #region Static Instance
    private static StimuliManager instance;
    public static StimuliManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<StimuliManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned StimuliManager", typeof(StimuliManager)).GetComponent<StimuliManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    private List<StimuliSensor> sensors = new List<StimuliSensor>();
    private List<Character> characters = new List<Character>();

    public void RegisterCharacter(Character sensor)
    {
        characters.Add(sensor);
    }

    public void RegisterSensor(StimuliSensor sensor)
    {
        sensors.Add(sensor);
    }

    public void BroadcastStimuli(string stimuli, GameObject source)
    {
        foreach(StimuliSensor sensor in sensors)
        {
            if (sensor != null)
            {
                sensor.Stimuli(stimuli, source);
            }
        }
        foreach (Character character in characters)
        {
            if (character != null)
            {
                character.Stimuli(stimuli, source);
            }
        }
    }
}
