﻿using System.Collections.Generic;
using UnityEngine;

namespace mw.player
{
    public abstract class CharacterState : MonoBehaviour
    {

        protected Character character;
        private bool ended;
        private bool sentencedToEnd;
        protected Dictionary<string, CharacterStateTransition> transitionsDict = new Dictionary<string, CharacterStateTransition>();

        public GameObject[] ObjectsWhileActive=new GameObject[0];

        public CharacterStateTransition[] transitions;
        public TriggerInterface OnEnterStateTrigger;
        public TriggerInterface OnResumeStateTrigger;
        public TriggerInterface OnPauseStateTrigger;
        public TriggerInterface OnExitStateTrigger;
        public OnStimuliTrigger[] StimuliTriggers = new OnStimuliTrigger[0];

        [HideInInspector]
        public TriggerInterface TriggerSource;

        protected bool Ended { get => ended; set => ended = value; }

        public bool HasEnded() {
            return Ended;
        }

        private void Awake()
        {
            foreach(CharacterStateTransition transition in transitions)
            {
                transitionsDict.Add(transition.Name, transition);
            }
        }

        public void SetEndSentence(bool endFlag)
        {
            sentencedToEnd = endFlag;
        }

        public CharacterStateTransition GetTransition(string name)
        {
            if (!transitionsDict.ContainsKey(name))
            {
                return null;
            }
            return transitionsDict[name];
        }

        public Character GetCharacter()
        {
            return character;
        }

        protected void SetEnabledObjectsWhileActive(bool enabled)
        {
            foreach(GameObject obj in ObjectsWhileActive)
            {
                obj.SetActive(enabled);
            }
        }

        public void OnEnterState(Character character)
        {
            this.character = character;
            sentencedToEnd = false;
            ended = false;
            SetEnabledObjectsWhileActive(true);
            if (OnEnterStateTrigger != null)
            {
                OnEnterStateTrigger.Fire();
            }
            OnEnterState();
        }

        public abstract void OnEnterState();

        public void PauseState()
        {
            if (OnPauseStateTrigger != null)
            {
                OnPauseStateTrigger.Fire();
            }
            SetEnabledObjectsWhileActive(false);
            OnPauseState();
        }

        public abstract void OnPauseState();

        public void ResumeState()
        {
            SetEnabledObjectsWhileActive(true);
            if (OnResumeStateTrigger != null)
            {
                OnResumeStateTrigger.Fire();
            }
            OnResumeState();
        }

        public abstract void OnResumeState();

        public void Stimuli(string stimuli, GameObject source)
        {
            if (!enabled || !gameObject.activeSelf)
            {
                return;
            }
            foreach (OnStimuliTrigger trigger in StimuliTriggers)
            {
                if (trigger.Stimuli.Equals(stimuli))
                {
                    trigger.Trigger.EventSource = source;
                    trigger.Trigger.Fire();
                }
            }
            OnStimuli(stimuli, source);
        }

        public abstract void OnStimuli(string stimuli, GameObject source);

        public bool CanBeActivated()
        {
            return CanBeActivated(character);
        }

        public virtual bool CanBeActivated(Character _character)
        {
            return true;
        }

        public abstract void OnLateUpdate();

        public void CallOnLateUpdate()
        {
            OnLateUpdate();
        }

        public void CallOnUpdate()
        {
            if (sentencedToEnd)
            {
                character.EndState();
                return;
            }
            OnUpdate();
        }

        public abstract void OnUpdate();

        public void CallOnFixedUpdate()
        {
            if (sentencedToEnd)
            {
                character.EndState();
                return;
            }
            OnFixedUpdate();
        }

        public abstract void OnFixedUpdate();

        public void ExitState()
        {
            if (OnExitStateTrigger != null)
            {
                OnExitStateTrigger.Fire();
            }
            SetEnabledObjectsWhileActive(false);
            OnExitState();
        }

        public abstract void OnExitState();

        public void EndState()
        {
            this.character = null;
            Ended = true;
        }

    }
}