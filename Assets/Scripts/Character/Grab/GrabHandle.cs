﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHandle : MonoBehaviour
{

    public TriggerInterface OnGrab;
    public TriggerInterface OnRelease;

    protected Grabbable target;
    protected Transform prevParent;
    protected Rigidbody targetBody;

    public Grabbable getTarget()
    {
        return target;
    }

    public void Grab(Grabbable grabbable)
    {
        if (target != null)
        {
            return;
        }
        target = grabbable;
        targetBody = target.GetComponent<Rigidbody>();
        if (targetBody != null)
        {
            targetBody.isKinematic = true;
            targetBody.detectCollisions = false;
        }
        prevParent = target.transform.parent;
        target.handle = this;
        target.transform.parent = transform;
        target.transform.localPosition = Vector3.zero;
        if (target.OnGrab != null)
        {
            target.OnGrab.Fire();
        }
        if (OnGrab != null)
        {
            OnGrab.Fire();
        }
    }

    public void Release()
    {
        if (target == null)
        {
            return;
        }
        if (targetBody != null)
        {
            targetBody.isKinematic = false;
            targetBody.detectCollisions = true;
        }
        target.transform.parent = prevParent;
        target.handle = null;
        if (target.OnRelease != null)
        {
            target.OnRelease.Fire();
        }
        if (OnRelease != null)
        {
            OnRelease.Fire();
        }
        target = null;
    }

}
