﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    public Slider slider;
    public float MaxHealth=100;
    public float InitialHealth=100;
    public TriggerInterface OnDamageTrigger;
    public TriggerInterface OnDeathTrigger;
    
    //Es necesario asignarlo manualmente
    public SetDamageToTextTrigger damageTrigger;

    protected float CurrentHealth;

    [HideInInspector]
    public GameObject damageSource;
    [HideInInspector]
    public float damageAmountTaken;

    void Start()
    {             
        CurrentHealth = InitialHealth;
        if (slider != null)
        {
            slider.maxValue = MaxHealth;
            slider.value = CurrentHealth;
        }
    }
    

    public void SetHealth(float amount)
    {        
        CurrentHealth = amount;
        RunValidations();
    }

    public void ReceiveDirectDamage(float amount, GameObject source)
    {
        damageAmountTaken = amount;
        damageSource = source;
        damageTrigger.damage = amount;
        CurrentHealth -= amount;
        if (OnDamageTrigger != null)
        {
            OnDamageTrigger.EventSource = source;
            OnDamageTrigger.Fire();
        }
        RunValidations(); 
    }

    protected void RunValidations()
    {
        if (CurrentHealth <= 0 && OnDeathTrigger != null)
        {
            OnDeathTrigger.Fire();
        }
        if (slider != null)
        {
            slider.value = CurrentHealth;
        }
    }
}
