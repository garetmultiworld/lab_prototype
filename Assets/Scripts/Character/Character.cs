﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace mw.player
{
    public class Character : MonoBehaviour, IStimuliSensor
    {

        public CharacterState BaseState;

        private CharacterState CurrentState;

        public GameObject ParentOverride = null;

        private Stack<CharacterState> States=new Stack<CharacterState>();

        public Inventory[] inventories = new Inventory[0];

        protected Dictionary<string, Inventory> Inventories = new Dictionary<string, Inventory>();

        [SerializeField]
        public bool MovementAllowed = true;

        public Character()
        {
            InitInventories();
        }

        protected void Awake()
        {
            if (BaseState != null)
            {
                SetUpCurrentState(BaseState);
            }
            StimuliManager.Instance.RegisterCharacter(this);
        }

        public GameObject GetGameObject()
        {
            if (ParentOverride != null)
            {
                return ParentOverride;
            }
            return gameObject;
        }

        private void InitInventories()
        {
            Inventory instance;
            Inventory[] newArr = new Inventory[inventories.Length];
            int arrIndex = 0;
            foreach (Inventory reference in inventories)
            {
                instance = InventoryManager.Instance.getInventory(reference);
                Inventories.Add(reference.Name, instance);
                newArr[arrIndex] = instance;
                arrIndex++;
            }
            inventories = newArr;
        }

        public CharacterState GetCurrentState()
        {
            return CurrentState;
        }

        public Inventory GetInventory(string name)
        {
            return Inventories[name];
        }

        public bool CanMove()
        {
            return MovementAllowed;
        }

        public float GetMovementSpeed()
        {
            return 0;
        }

        public void Stimuli(string stimuli, GameObject source)
        {
            if (CurrentState != null)
            {
                CurrentState.Stimuli(stimuli, source);
            }
        }
        
        void Update()
        {
            if (CurrentState != null) {
                while (!CurrentState.gameObject.activeSelf && States.Count > 0)
                {
                    EndState();
                }
                CurrentState.CallOnUpdate();
            }
        }
        
        void LateUpdate()
        {
            if (CurrentState != null)
            {
                while (!CurrentState.gameObject.activeSelf && States.Count > 0)
                {
                    EndState();
                }
                CurrentState.CallOnLateUpdate();
            }
        }

        void FixedUpdate()
        {
            if (CurrentState != null)
            {
                while (!CurrentState.gameObject.activeSelf && States.Count > 0)
                {
                    EndState();
                }
                CurrentState.CallOnFixedUpdate();
            }
        }

        private void SetCurrentState(CharacterState NextState)
        {
            States.Push(CurrentState);
            SetUpCurrentState(NextState);
        }

        private void SetUpCurrentState(CharacterState NextState)
        {
            CurrentState = NextState;
            NextState.OnEnterState(this);
        }

        public void ChangeState(CharacterState NextState)
        {
            if (NextState != null && NextState.gameObject.activeSelf && NextState.CanBeActivated(this))
            {
                CurrentState.PauseState();
                SetCurrentState(NextState);
            }
        }

        public void EndState()
        {
            if (States.Count > 0)
            {
                CurrentState.ExitState();
                CurrentState.EndState();
                CurrentState = States.Pop();
                CurrentState.ResumeState();
            }
        }

        public static implicit operator GameObject(Character v)
        {
            return v.gameObject;
        }

    }
}