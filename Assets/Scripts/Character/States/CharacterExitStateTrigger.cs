﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterExitStateTrigger : TriggerInterface
{

    public CharacterState ParentState;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        ParentState.ExitState();
    }

}
