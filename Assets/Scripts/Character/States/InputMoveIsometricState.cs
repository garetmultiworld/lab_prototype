﻿using mw.player;
using UnityEngine;

public class InputMoveIsometricState : CharacterState
{

    public string xAxis;
    public string yAxis;
    public bool flip;
    public float initialScaleX;
    public GameObject ObjectToFlip;

    protected Animator animator;
    protected Rigidbody2D body=null;

    private bool isMoving=false;

    public override void OnEnterState()
    {
        if (body==null)
        {
            body = character.GetComponent<Rigidbody2D>();
            animator = character.GetComponent<Animator>();
        }
    }

    public override void OnExitState()
    {
    }

    public override void OnFixedUpdate()
    {
    }

    public override void OnPauseState()
    {
    }

    public override void OnResumeState()
    {
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
    }

    public override void OnUpdate()
    {
        if (character.CanMove())
        {
            float xInput = Input.GetAxis(xAxis);
            float yInput = Input.GetAxis(yAxis);
            if (flip)
            {
                if (xInput < 0)
                {
                    ObjectToFlip.transform.localScale = new Vector3(
                        -initialScaleX,
                        ObjectToFlip.transform.localScale.y,
                        ObjectToFlip.transform.localScale.z
                    );
                }
                else if (xInput > 0)
                {
                    ObjectToFlip.transform.localScale = new Vector3(
                        initialScaleX,
                        ObjectToFlip.transform.localScale.y,
                        ObjectToFlip.transform.localScale.z
                    );
                }
            }
            isMoving = (xInput != 0 || yInput != 0);
            if (isMoving)
            {
                float speed= character.GetMovementSpeed();
                Vector3 moveVector = new Vector3(xInput, yInput, 0);
                body.MovePosition(new Vector2(
                    (transform.position.x + moveVector.x * speed * Time.deltaTime),
                    transform.position.y + moveVector.y * speed * Time.deltaTime)
                );
                if (animator != null)
                {
                    animator.SetFloat("xInput", xInput);
                    animator.SetFloat("yInput", yInput);
                }
            }
            else
            {
                character.EndState();
            }
        }
        if (animator != null)
        {
            animator.SetBool("isMoving", isMoving);
        }
    }
    public override void OnLateUpdate()
    {

    }
}
