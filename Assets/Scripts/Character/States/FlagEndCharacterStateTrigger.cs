﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagEndCharacterStateTrigger : TriggerInterface
{

    public CharacterState characterState;

    public override void Cancel()
    {
        characterState.SetEndSentence(false);
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        characterState.SetEndSentence(true);
    }

}
