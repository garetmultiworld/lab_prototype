﻿using mw.player;

public class TriggerPlayerEvent : TriggerInterface
{

    public int PlayerNumber = 1;
    public Player.Events Evento;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        PlayerManager.Instance.ProcessPlayerEvent(PlayerNumber - 1, Evento);
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Evento.ToString() + ")";
        return desc;
    }

}
