﻿using mw.singleton;
using System.Collections.Generic;
using UnityEngine;

namespace mw.player
{
    public class Player : Character
    {

        public TriggerInterface[] OnEnableMovement=new TriggerInterface[0];
        public TriggerInterface[] OnDisableMovement=new TriggerInterface[0];

        public enum Events
        {
            EnableMovement,
            DisableMovement
        }

        public enum Actions
        {

        }

        public int Number = 0;
        protected int index = 0;

        new void Awake()
        {
            base.Awake();
            SingletonManager.InitSingletons();
            PlayerManager.Instance.RegisterPlayer(this, Number);
        }

        public void SetIndex(int index)
        {
            this.index = index;
        }

        public void ProcessEvent(Player.Events evento)
        {
            switch (evento)
            {
                case Events.DisableMovement:
                    MovementAllowed = false;
                    foreach (TriggerInterface trigger in OnDisableMovement)
                    {
                        if (trigger != null)
                        {
                            trigger.Fire();
                        }
                    }
                    break;
                case Events.EnableMovement:
                    MovementAllowed = true;
                    foreach (TriggerInterface trigger in OnEnableMovement)
                    {
                        if (trigger != null)
                        {
                            trigger.Fire();
                        }
                    }
                    break;
            }
        }

        internal void ProcessAction(Actions action)
        {            
            switch(action)
            {
                
            }
            
        }
    }
}