﻿using System.Collections;
using UnityEngine;

public class DamageOnTouch : MonoBehaviour
{

    public TriggerInterface OnHitTrigger;
    public float Amount=1;
    public LayerMask FilterLayer;
    public float ReloadTime=0.3f;
    [HideInInspector]
    public bool canAttack = false;
    public float weaponAttackRatio = 0.5f;
    public float weaponComboDuration = 1.5f;
    [HideInInspector]
    public bool keepDamage = false;
    float ratio;
    float comboDuration;
    Health health;
    [HideInInspector]
    public bool playerHit = false;
    Collider enemyCollider;

    private void Start()
    {
        ratio = weaponAttackRatio;
        comboDuration = weaponComboDuration;
    }
    private void Update()
    {
        Debug.Log("can Attack is: " + canAttack);
        Debug.Log("keepDamage is: " + keepDamage);
        if (playerHit==false)
        {
            keepDamage = false;
        }
    }
    public void OnTriggerEnter(Collider c)
    {       
        if (!canAttack)
        {
            return;
        }
        health = c.GetComponent<Health>();
        enemyCollider = c;  
    }

    void InflictDamage(bool inRange)
    {
        if (inRange)
        {
            if (health != null && (((1 << enemyCollider.gameObject.layer) & FilterLayer) != 0) && canAttack)
            {
                playerHit = true;
                health.ReceiveDirectDamage(Amount, gameObject);
                StartCoroutine(Reload());
                if (OnHitTrigger != null)
                {
                    OnHitTrigger.Fire();
                }
            }
        }
        else
        {
            return;
        }
    }
    /*private void OnTriggerStay(Collider other)
    {
        if (!canAttack)
        {
            return;
        }
        if (keepDamage == true && health != null && (((1 << other.gameObject.layer) & FilterLayer) != 0) && canAttack)
        {
            Debug.Log("OnTriggerStay enter !");
            health = other.GetComponent<Health>();
            ratio -= Time.deltaTime;
            comboDuration -= Time.deltaTime;
            if (ratio <= 0)
            {                
                health.ReceiveDirectDamage(Amount, gameObject);
                StartCoroutine(Reload());
                if (OnHitTrigger != null)
                {
                    OnHitTrigger.Fire();
                }
                ratio = weaponAttackRatio;
            }
            if (comboDuration <= 0)
            {
                ratio = weaponAttackRatio;
                comboDuration = weaponComboDuration;
                canAttack = false;
                keepDamage = false;
            }
        }
    }*/
    private IEnumerator Reload()
     {
         for(float currentTime = 0; currentTime < ReloadTime; currentTime+=Time.deltaTime)
         {
             yield return null;
         }
     }
}
