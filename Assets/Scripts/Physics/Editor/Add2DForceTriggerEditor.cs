using UnityEditor;

[CustomEditor(typeof(Add2DForceTrigger))]
public class Add2DForceTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(Add2DForceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "Add2DForceTrigger");
    }

    protected void StorePrefabConflict(Add2DForceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Add2DForceTrigger add2DForceTrigger = (Add2DForceTrigger)target;
        SetUpPrefabConflict(add2DForceTrigger);
        add2DForceTrigger.showAdd2DForceTrigger = EditorGUILayout.Foldout(
            add2DForceTrigger.showAdd2DForceTrigger,
            "2D Force"
        );
        if (add2DForceTrigger.showAdd2DForceTrigger)
        {
            add2DForceTrigger.body2D = EditorUtils.Rigidbody2DField(
                this,
                "Rigidbody 2D",
                add2DForceTrigger.body2D
            );
            add2DForceTrigger.direction = EditorUtils.Vector2Field(
                this,
                "Direction",
                add2DForceTrigger.direction
            );
            add2DForceTrigger.force = EditorUtils.FloatField(
                this,
                "Force",
                add2DForceTrigger.force
            );
        }
        StorePrefabConflict(add2DForceTrigger);
    }
}
