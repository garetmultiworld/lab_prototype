﻿using UnityEngine;

public class ChangeRigidbodyBodyTypeTrigger : TriggerInterface
{
    public bool IsKinematic;
    public Rigidbody TheRigidBody;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheRigidBody.isKinematic = IsKinematic;
    }

}
