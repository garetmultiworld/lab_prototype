﻿using UnityEngine;

public class UnPauseTrigger : TriggerInterface
{
    public override void Cancel()
    {
        Time.timeScale = 0;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Time.timeScale = 1;
    }
}
