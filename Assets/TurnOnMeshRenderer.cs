using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnMeshRenderer : MonoBehaviour
{
    MeshRenderer meshRenderer;
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
        meshRenderer.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
