using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour
{
    [Header ("Cosas que se deben asignar:")]
    public PlayerActiveState player;
    public GameObject DamageVolumeGameObject;


    //Evento para mover el player en el ataque 2 del Melee Combo
    public void Attack2Move()
    {
        player.Attack2Corroutine();
    }
    public void StartCombo()
    {
        player.ComboActive = true;
        player.canMove = false;
    }

    public void FinishCombo()
    {
        player.ComboActive = false;
        player.canMove = true;
        DamageVolumeGameObject.SetActive(false);
    }

    public void playerCanMove(int value)
    {
        switch (value)
        {
            case 0:               
                player.canMove = false;
                break;
            case 1:
                if (player.ComboActive)
                {
                    player.canMove = false;
                }
                else
                {
                    player.canMove = true;
                    DamageVolumeGameObject.SetActive(false);
                }                
                break;
        }
    }
}
